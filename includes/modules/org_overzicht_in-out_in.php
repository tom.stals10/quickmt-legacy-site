<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php $mutaties = getMutatieHandler($_SESSION['org'], "in");?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="font-weight-bold text-primary">Inkomsten (€<?php print(getTotaleMutaties($_SESSION['org'], "in")['total'] + 0); ?>)</h6>
    </div>
    <div class="card-body">
        <?php if(isset($_SESSION['overzicht_in_out_added']) && $_SESSION['overzicht_in_out_added'] == 'in'){ unset($_SESSION['overzicht_in_out_added']); unset($_POST); ?>
            <div class="card bg-success text-white shadow">
                <div class="card-body">
                    De transactie is succesvol toegevoegd
                </div>
            </div>
            <br>
        <?php }?>
        <?php if(isset($_SESSION['overzicht_in_out_removed']) && $_SESSION['overzicht_in_out_removed'] == 'in'){ unset($_SESSION['overzicht_in_out_removed']); unset($_POST); ?>
            <div class="card bg-success text-white shadow">
                <div class="card-body">
                    De transactie is succesvol verwijderd
                </div>
            </div>
            <br>
        <?php }?>
        <?php if(hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.in_out.add")){ ?>
        <div class="card text-white shadow">
            <div class="card-body">
                <form method="POST" action="" class="user">
                    <div class="form-group">
                        <input autocomplete="off" type="text" class="form-control form-control-user" name="omschrijving" id="omschrijving" placeholder="Omschrijving" required><br>
                        <input autocomplete="off" type="text" class="form-control form-control-user" name="bedrag" id="bedrag" placeholder="Bedrag" required><br>
                        <button type="submit" class="btn btn-success btn-user btn-block" name="type" value="in">Toevoegen</button>
                    </div>
                </form>
            </div>
        </div>
        <hr>
        <?php } ?>
        <?php if(!empty($mutaties)){ ?>
            <div class="table">
                <table class="table table-bordered" id="inkostenTable">
                    <thead>
                    <tr>
                        <th style="width: 20%">Datum</th>
                        <th style="width: 60%">Omschrijving</th>
                        <th style="width: 20%">Bedrag</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($mutaties as $mutatie){ ?>
                        <tr>
                            <td><?php print date("d-m-Y", $mutatie['time']); ?></td>
                            <td><?php print $mutatie['omschrijving']; ?></td>
                            <td>€<?php print $mutatie['total']; ?></td>
                            <td>
                                <?php if(hasPerms($_SESSION['org'],$_SESSION['user_id'], "page.overzicht.in_out.remove") && $mutatie['time'] > getOrganisation($_SESSION['org'])['lastLoonReset']){ ?>
                                    <a href="/org/overzicht/inkomsten_uitgaven/delete/<?php print $mutatie['id']; ?>/in/" class="btn btn-sm btn-danger shadow-sm"><i class="fas fa-times fa-lg text-white"></i></a>
                                <?php } else { ?>
                                    <a class="btn btn-sm btn-secondary shadow-sm"><i class="fas fa-times fa-lg text-white"></i></a>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php }else{ ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    De inkomsten kunnen momenteel niet worden geladen
                </div>
            </div>
        <?php } ?>
    </div>
</div>