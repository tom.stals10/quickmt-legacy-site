<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Lonen</h1>
    <?php if(isPartner($_SESSION['org']) && partnerHasOpenPayment($_SESSION['org']) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.lonen.berekenen")){ ?>
        <a class="btn btn-md btn-primary shadow-sm disabled text-white">U kunt geen lonen berekenen tot dat de contributie aan Quick Company is betaald</a>
    <?php }elseif(orgHasPage($_SESSION['org'], 6) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.lonen.berekenen") && (getOrganisation($_SESSION['org'])['lastLoonReset'] == 0 || getOrganisation($_SESSION['org'])['lastLoonReset'] < time() - (60*60*24))){ ?>
        <a data-toggle="modal" data-target="#loonModal" class="btn btn-md btn-success text-white shadow-sm">Lonen Berekenen</a>
    <?php }elseif(hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.lonen.berekenen")){ ?>
        <a class="btn btn-md btn-primary shadow-sm disabled text-white">Het loon kan eenmalig per 24 uur worden berekend</a>
    <?php } ?>
</div>
<p class="font-weight-bold"><span class="text-danger">De informatie op deze pagina is vertrouwlijk en mag niet zomaar worden gedeeld</span></p>

<?php if(hasPendingDeletions($_SESSION['org']) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.admin.users")) { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-warning text-white shadow mb-3">
                <div class="card-body">
                    De volgende personen zijn aangevraagt om verwijderd te worden en zullen automatisch na de volgende loonberekening worden verwijderd. Deze gebruikers hebben al geen permissies meer op de website en kunnen dus nergens bij!
                    <ul>
                        <?php foreach (getAllUsersFromOrg($_SESSION['org']) as $user) { ?>
                            <?php if(!isPendingDeleted($user['id'], $_SESSION['org'])) continue?>
                            <li><?php print $user['name']; ?></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<?php } ?>