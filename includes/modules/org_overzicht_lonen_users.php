<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><hr>
<?php $users = getAllUsersFromOrg($_SESSION['org'])?>
<?php foreach ($users as $user) { ?>
    <?php $lonen = getAllLoonFromUser($_SESSION['org'],$user['id']); ?>
<?php if(!empty($lonen)){ ?>
        <div class="card shadow mb-4">
            <a href="#lonen_calc_<?php print $user['name'] ?>" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="lonen_calc_<?php print $user['name'] ?>">
                <h6 class="m-0 font-weight-bold text-primary"><?php print $user['name'] ?></h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse hide" id="lonen_calc_<?php print $user['name'] ?>">
                <div class="card-body">
                    <div class="row">
                        <div class="table">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 10%">ID</th>
                                    <th style="width: 20%">Datum</th>
                                    <th style="width: 30%">Berekend Door</th>
                                    <th style="width: 20%">Bedrag</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($lonen as $loon){ ?>
                                    <tr>
                                        <td><?php print $loon['id']; ?></td>
                                        <td><?php print date("d-m-Y H:i:s", $loon['time']); ?></td>
                                        <td><?php print getUserInfo($loon['calculatedBy'])['username']; ?> (<?php print getUserInfo($loon['calculatedBy'])['name']; ?>)</td>
                                        <td>€<?php print $loon['total'] + 0; ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>