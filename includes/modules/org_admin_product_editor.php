<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

if(hasPerms($_SESSION['org'], $_SESSION['user_id'], 'page.admin.products.manage')) {
    if(isset($_POST['username'], $_POST['price'])){
        $username = ltrim(rtrim(strip_tags($_POST['username'])));
        if(isset($_POST['icon'])){
            $icon = ltrim(rtrim(strip_tags($_POST['icon'])));
        }else{
            $icon = ":x:";
        }

        if(isset($_POST['limit']) && is_numeric($_POST['limit'])){
            $limit = ceil($_POST['limit']);
        }else{
            $limit = 0;
        }

        if(isset($_POST['isLimited'])){
            $isLimited = $_POST['isLimited'];
        }else{
            $isLimited = 0;
            $limit = 0;
        }


        $price = $_POST['price'];

        if($username == ""){
            header('Location: /org/admin/products/error/');
            exit();
        }

        if($icon == ""){
            header('Location: /org/admin/products/error/');
            exit();
        }

        if(!is_numeric($price)){
            header('Location: /org/admin/products/error/');
            exit();
        }

        updateProduct($request[3], $username, $icon, $price, $isLimited,$limit);

        addLog($_SESSION['user_id'], "Succesvolle het product $request[3] aangepast");

        print_r($_POST);
        header('Location: /org/admin/products/'.$request[3].'/succes/');
        exit();
    }elseif(isset($request[4]) && $request[4] == "delete" && hasPerms($_SESSION['org'], $_SESSION['user_id'], 'page.admin.products.manage')){
        deleteProduct($request[3]);
        addLog($_SESSION['user_id'], "Succesvolle het product $request[3] verwijderd");
        header('Location: /org/admin/products/succes_deleted/');
        exit();
    }
}
?>
<?php $product = getProductByID($request[3]); ?>
<?php if(isset($request[4]) && $request[4] == "succes"){ ?>
    <div class="card bg-success text-white shadow">
        <div class="card-body">
            U heeft succesvol een product aangepast
        </div>
    </div>
    <br>
<?php }?>
<div class="row">
    <div class="col-12 card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="font-weight-bold text-primary">Product Aanpassen</h6>
        </div>
        <div class="card-body">
            <div class="card text-white shadow">
                <div class="card-body">
                    <form method="POST" class="user" name="orgAddProductID" id="editProduct">
                        <div class="form-group">
                            <label class="text-dark" for="username">Product Naam</label>
                            <input autocomplete="off" type="text" class="form-control" name="username" id="username"  value="<?php print $product['name'] ?>"  required>
                        </div>
                        <?php if(!isPartner($_SESSION['org'])){ ?>
                            <div class="form-group">
                                <label class="text-dark" for="icon">Discord Icon</label>
                                <input autocomplete="off" type="text" class="form-control" name="icon" id="icon"  value="<?php print $product['icon'] ?>"  required>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <label class="text-dark" for="icon">Gelimiteerd Product</label>
                            <select autocomplete="off"  required class="form-control" name="isLimited">
                                <option <?php if(productIsDailyLimited($request[3])){ print "selected"; } ?> value=1>Ja</option>
                                <option <?php if(!productIsDailyLimited($request[3])){ print "selected"; } ?> value=0>Nee</option>
                            </select>
                        </div>
                        <?php if(productIsDailyLimited($request[3])){ ?>
                            <div class="form-group">
                                <label class="text-dark" for="username">Dagelijks Limiet (Per Klant)</label>
                                <input autocomplete="off" type="text" class="form-control" name="limit" id="limit"  value="<?php print $product['limit'] ?>"  required>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <label class="text-dark" for="price">Bedrag</label>
                            <input autocomplete="off" type="text" class="form-control" name="price" id="price" value="<?php print $product['price'] ?>" required>
                        </div>
                        <a class="btn btn-success text-white" onclick="document.getElementById('editProduct').submit();">Aanpassen</a>
                    </form>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>