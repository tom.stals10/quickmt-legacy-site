<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><body class="bg-gradient-primary">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Selecteer een organisatie!</h1>
                                </div>
                                <form method="POST" action="/includes/auth/process_org.php" class="user" name="org_form">
                                    <div class="form-group">
                                        <select required class="form-control" name="orgSelect">
                                            <?php foreach (getAllOrganisations() as $org){ ?>
                                                <?php if (!hasAccess($org['id'], $_SESSION['user_id'])) continue; ?>
                                                <?php if ($org['active'] != 1) continue; ?>
                                                <?php if($org['active'] == 1){ ?>
                                                    <option value="<?php print $org['id']; ?>"><?php print $org['name'] . " - " . $org['location']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <input type="button" value="Doorgaan" class="btn btn-primary btn-user btn-block" onclick="this.form.submit()">
                                </form>
                                <br>
                                <?php if(isset($request[0]) && $request[0] == "error"){ ?>
                                    <div class="card bg-danger text-white shadow">
                                        <div class="card-body">
                                            Er is iets mis gegaan bij het selecteren van een organisatie
                                        </div>
                                    </div>
                                <?php }else{ ?>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>


