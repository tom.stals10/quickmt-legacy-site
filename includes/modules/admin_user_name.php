<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php $user = getUserInfo($request[2]); ?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Minecraftnaam Wijzigen</h6>
    </div>
    <div class="card-body">
        <p>De gebruiker kan zelf zijn Minecraftnaam wijzigen door naar zijn profielpagina te gaan. Hij komt hier door linksbovenin op zijn naam te klikken en hierna te klikken op de knop Profiel.</p>
    </div>
</div>