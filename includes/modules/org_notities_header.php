<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Notities</h1>
    <?php if(hasPerms($_SESSION['org'],$_SESSION['user_id'], "page.notities.add")){ ?>
        <a data-toggle="modal" data-target="#addNote" class="btn btn-md btn-primary shadow-sm text-white">Notitie Toevoegen</a>
    <?php } ?>
</div>
<p class="font-weight-bold"><span class="text-danger">De notities zijn voor iedereen binnen de organisatie zichtbaar. Denk dus eerst goed na voordat je een notitie plaatst</span></p>
