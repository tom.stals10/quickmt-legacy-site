<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><div class="row">
    <div class="col-xl-10 col-lg-10">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="font-weight-bold text-primary">Omzet (Per Loonsberekening)</h6>
            </div>
            <div class="card-body">
                <div class="chart-area">
                    <canvas id="myAreaChart"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-2 col-lg-2">
        <?php $in = round(getTotaleMutaties($_SESSION['org'], "in")['total'] + 0,1); ?>
        <?php $uit = round(getTotaleMutaties($_SESSION['org'], "out")['total'] + 0,1); ?>
        <?php $kas = round($in - $uit,1); ?>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="font-weight-bold text-primary">Kas (Huidig)</h6>
            </div>
            <div class="card-body">
                <?php if($kas <= 0){ ?>
                    <p class="h3 font-weight-bold text-danger">€<?php print $kas; ?></p>
                <?php }else{ ?>
                    <p class="h3 font-weight-bold text-success">€<?php print $kas; ?></p>
                <?php } ?>
            </div>
        </div>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="font-weight-bold text-primary">Inkomsten (Huidig)</h6>
            </div>
            <div class="card-body">
                <?php if($in <= 0){ ?>
                    <p class="h3 font-weight-bold text-danger">€<?php print $in; ?></p>
                <?php }else{ ?>
                    <p class="h3 font-weight-bold text-success">€<?php print $in; ?></p>
                <?php } ?>
            </div>
        </div>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="font-weight-bold text-primary">Uitgaven (Huidig)</h6>
            </div>
            <div class="card-body">
                <?php if($uit <= 0){ ?>
                    <p class="h3 font-weight-bold text-danger">€<?php print $uit; ?></p>
                <?php }else{ ?>
                    <p class="h3 font-weight-bold text-success">€<?php print $uit; ?></p>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<script>
    $( document ).ready(function() {
        $.getJSON('/api/getKas.php?org=<?php print $_SESSION['org'] ?>', function(result){
            $.each(result, function (key, val) {
                var values = val.split('=');
                localStorage.setItem('omzet_'+key, values[0]);
                localStorage.setItem('time_'+key, values[1]);
                localStorage.setItem('kas_'+key, values[2]);
            });
        });
    });
</script>