<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><div class="modal fade" id="loonModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Weet u zeker dat u de lonen wilt bereken?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Het is verstandig eerst de actuele lonen te bekijken voordat u doorgaat. Mocht er een fout zitten in de loon berekening dan is dit niet terug te draaien wanneer u doorgaat<br><br>De berekend wordt uitgevoerd voor de organisatie: <br><span class="font-weight-bold"><?php print getOrganisation($_SESSION['org'])['name']; ?> - <?php print getOrganisation($_SESSION['org'])['location']; ?></span></div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Annuleren</button>
                <a class="btn btn-primary" href="/org/overzicht/lonen/calculate/">Doorgaan</a>
            </div>
        </div>
    </div>
</div>