<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php if(isset($request[3]) && $request[3] == "empty_user") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    Niet alle velden waren juist ingevuld
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($request[3]) && $request[3] == "user_added") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-success text-white shadow mb-3">
                <div class="card-body">
                    De gebruiker is succesvol toegevoegd
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($request[3]) && $request[3] == "you") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    Je kan jezelf niet toevoegen en/of verwijderen
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($request[3]) && $request[3] == "user_removed") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-success text-white shadow mb-3">
                <div class="card-body">
                    De gebruiker wordt automatisch naar de volgende keer loon berekenen verwijderd. Zijn permissies zijn al weg gehaald dus hij kan nergens bij.
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($request[3]) && $request[3] == "unknown_user") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    De gebruiker kon niet worden gevonden
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($request[3]) && $request[3] == "user_is_admin") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    De geselecteerde gebruiker is een administrator
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($request[3]) && $request[3] == "no_perms") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    Je hebt niet de juiste permissies
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($request[3]) && $request[3] == "already_added") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    De geselecteerde gebruiker is al lid van deze organisatie
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($request[3]) && $request[3] == "not_added") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    De geselecteerde gebruiker is geen lid van deze organisatie
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php if(hasPendingDeletions($_SESSION['org'])) { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-warning text-white shadow mb-3">
                <div class="card-body">
                    De volgende personen zijn aangevraagt om verwijderd te worden en zullen automatisch na de volgende loonberekening worden verwijderd. Deze gebruikers hebben al geen permissies meer op de website en kunnen dus nergens bij!
                    <ul>
                        <?php foreach (getAllUsersFromOrg($_SESSION['org']) as $user) { ?>
                            <?php if(!isPendingDeleted($user['id'], $_SESSION['org'])) continue?>
                            <li><?php print $user['name']; ?></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<?php } ?>


<?php $users = getAllUsersFromOrg($_SESSION['org']);?>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="font-weight-bold text-primary">Gebruikers</h6>
        </div>
        <div class="card-body">
            <?php if(!empty($users)){ ?>
                <div class="table">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 10%">ID</th>
                                    <th style="width: 20%">Username</th>
                                    <th style="width: 20%">Naam</th>
                                    <th style="width: 50%">Loonpercentage</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($users as $user){ ?>
                                    <?php if(isAdmin($user['id'])) continue; ?>
                                <tr>
                                    <td><?php print $user['id']; ?></td>
                                    <td><?php print $user['username']; ?></td>
                                    <td><?php print $user['name']; ?></td>
                                    <td><?php print getLoonPercentage($_SESSION['org'], $user['id']) * 100 . "%"; ?></td>
                                    <td>
                                        <?php if(isAdmin($user['id']) || !hasPerms($_SESSION['org'],$_SESSION['user_id'], "page.admin.users.manage") || isPendingDeleted($user['id'], $_SESSION['org'])){ ?>
                                            <a class="btn btn-sm btn-secondary shadow-sm"><i class="fas fa-times fa-lg text-white"></i></a>
                                        <?php } else { ?>
                                            <a href="/org/admin/users/<?php print $user['id']; ?>/" class="btn btn-sm btn-primary shadow-sm"><i class="fas fa-edit fa-lg text-white"></i></a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
            <?php }else{ ?>
                <div class="card bg-danger text-white shadow">
                    <div class="card-body">
                        De gebruikers kunnen momenteel niet worden geladen
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>


