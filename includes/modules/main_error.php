<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php function printMainError($error, $loggedIn){ ?>
<?php $fileArray = explode('/', $error['file']); ?>
<body class="bg-gradient-primary">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Onbekende Fout</h1>
                                    <P class="text-gray-900 mb-4">Er is zojuist een niet oplosbare fout opgetreden op de website. Mocht dit de eerste keer zijn dat u dit bericht ziet reload de pagina dan een keer. Mocht u dit bericht nu al voor de 2e keer of vaker zien neem dan even contact op met Julian Meurer via <span class="font-weight-bold">1</span> van de onderstaande contact mogelijkheden</P>
                                    <p class="text-gray-800 mb-4">
                                        Discord: <span class="font-weight-bold">ItsJulian#8185</span><br>
                                    </p>
                                </div>
                                <div class="card bg-secondary text-white shadow">
                                    <div class="card-body">
                                        Bestand: <span class="font-weight-bold"><?php print end($fileArray); ?></span><br>
                                        Regel: <span class="font-weight-bold"><?php print $error['line'] ?></span><br>
                                        Bericht: <span class="font-weight-bold"><?php print $error['message'] ?></span><br>
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php } ?>
