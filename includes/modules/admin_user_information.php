<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php $user = getUserInfo($request[2]); ?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary"><?php print getLanguages("admin_user_information", "user_info"); ?>Gebruiker Informatie</h6>
    </div>
    <div class="card-body">
        <?php if (!empty($user) >= 1){ ?>
            <p><span class="font-weight-bold">Medewerker ID:</span> <?php print $user['id']; ?></p>
            <p><span class="font-weight-bold">Username:</span> <?php print $user['username']; ?></p>
            <p><span class="font-weight-bold">Minecraftnaam:</span> <?php print $user['name']; ?></p>
            <p><span class="font-weight-bold">Gekoppelde Bedrijven: </span><?php print getAmmountOfUserOrgs($user['id']); ?></p>
            <?php if($user['active'] != 1){ ?>
                <p><span class="font-weight-bold">Verwijderd Op:</span> <?php print date("d-m-Y H:i:s", $user['deletedOn']); ?></p>
                <p><span class="font-weight-bold">Verwijderd Door:</span> <?php print getUserInfo($user['deletedBy'])['username'] . " (".getUserInfo($user['deletedBy'])['name'].")"; ?></p>
            <?php } else { ?>
                <p><span class="font-weight-bold">Persoonlijke Code:</span> <?php print $user['code']; ?></p>
                <p><span class="font-weight-bold">Aangemaakt Op:</span> <?php print date("d-m-Y H:i:s", $user['createdOn']); ?></p>
                <p><span class="font-weight-bold">Aangemaakt Door:</span> <?php print getUserInfo($user['createdBy'])['username'] . " (".getUserInfo($user['createdBy'])['name'].")"; ?></p>
                <p><span class="font-weight-bold">Laatste Code Reset:</span> <?php print date("d-m-Y H:i:s", $user['lastCodeReset']); ?></p>
                <p><span class="font-weight-bold">Laatste Minecraftnaam Aanpassing:</span> <?php print date("d-m-Y H:i:s", $user['lastNameUpdate']); ?></p>
                <p><span class="font-weight-bold">Laatste Wachtwoord Wijziging:</span> <?php print date("d-m-Y H:i:s", $user['lastPasswordUpdate']); ?></p>
            <?php }?>
        <?php } else { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    De geselecteerde gebruiker kan momenteel niet worden geladen
                </div>
            </div>
        <?php } ?>
        <?php if(isActiveUser($user['id']) && !isAdmin($user['id'])){ ?>
            <?php if(getAmmountOfUserOrgs($user['id']) >= 1){ ?>
                <p class="text-danger font-weight-bold">Het is niet mogelijk een gebruiker te verwijderen zolang deze nog aan 1 of meer bedrijven staat gekoppeld</p>
            <?php }else{ ?>
                <form method="POST" action="/includes/auth/process_deleteuser.php" class="user" name="adminUserDelete_form" id="adminUserDelete_form">
                    <div class="form-group">
                        <button type="submit" class="btn btn-md btn-primary shadow-sm" name="usr" id="usr" value="<?php print $user['id']; ?>">Gebruiker Verwijderen</button>
                    </div>
                </form>
            <?php } ?>
        <?php }elseif(!isAdmin($user['id'])){ ?>
            <form method="POST" action="/includes/auth/process_activateuser.php" class="user" name="adminUserDelete_form" id="adminUserDelete_form">
                <div class="form-group">
                    <button type="submit" class="btn btn-md btn-success shadow-sm" name="usr" id="usr" value="<?php print $user['id']; ?>">Gebruiker Activeren</button>
                </div>
            </form>
        <?php } ?>
    </div>
</div>