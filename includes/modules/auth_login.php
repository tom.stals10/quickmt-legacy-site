<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><body class="bg-gradient-primary">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Welkom Terug!</h1>
                                </div>
                                <form method="POST" action="/includes/auth/process_login.php" class="user" name="login_form">
                                    <div class="form-group">
                                        <input autocomplete="off" type="text" class="form-control form-control-user" name="username" id="username" placeholder="Username">
                                    </div>
                                    <div class="form-group">
                                        <input autocomplete="off" type="password" class="form-control form-control-user" name="password" id="password" placeholder="Wachtwoord">
                                    </div>
                                    <div class="form-group">
                                        <input autocomplete="off" type="number" class="form-control form-control-user" name="medwcode" id="medwcode" placeholder="Persoonlijke Code">
                                    </div>
                                    <input type="button" class="btn btn-primary btn-user btn-block" value="Inloggen" onclick="formhash(this.form, this.form.password);" /> 
                                </form>
                                <br>
                                
                                <?php if(isset($request[0]) && $request[0] == "error"){ ?>
                                <div class="card bg-danger text-white shadow">
                                    <div class="card-body">
                                        Verkeerde gebruikersnaam en/of wachtwoord!
                                    </div>
                                </div>
                                <?php } ?>
                                <?php if(isset($request[0]) && $request[0] == "inactive"){ ?>
                                    <div class="card bg-danger text-white shadow">
                                        <div class="card-body">
                                            Uw account is niet meer actief
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if(isset($request[0]) && $request[0] == "password_reset"){ ?>
                                    <div class="card bg-success text-white shadow">
                                        <div class="card-body">
                                            Uw wachtwoord is succesvol gereset!
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    if (localStorage.getItem("username") !== null) {
        document.getElementById("username").value = localStorage.getItem("username");
    }
    if (localStorage.getItem("token") !== null) {
        document.getElementById("medwcode").value = localStorage.getItem("token");
    }
</script>


