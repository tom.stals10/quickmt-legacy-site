<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Persoonlijke Doelen</h1>
</div>
<p class="font-weight-bold"><span class="text-danger">Als een doel wordt gewijzigd kan dit niet worden teruggedraaid</span></p>
<?php if(isset($_SESSION['doel_changed']) && $_SESSION['doel_changed'] == "true") { unset($_SESSION['doel_changed']); ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-success text-white shadow mb-3">
                <div class="card-body">
                    Het doel is succesvol aangepast
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($_SESSION['doel_changed']) && $_SESSION['doel_changed'] == "false") { unset($_SESSION['doel_changed']); ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    Het doel kon niet worden aangepast
                </div>
            </div>
        </div>
    </div>
<?php } ?>
