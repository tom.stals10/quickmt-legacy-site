<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php if(isset($request[2]) && $request[2] == "user_error") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    De nieuwe gebruiker kan momenteel niet worden gemaakt
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($request[2], $_SESSION['TMP_username']) && $request[2] == "users_succes") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-success text-white shadow mb-3">
                <div class="card-body">
                    De nieuwe gebruiker is succesvol aangemaakt
                    <br>
                    <p><span class="font-weight-bold">Username:</span> <?php print $_SESSION['TMP_username']; ?><br>
                    <span class="font-weight-bold">Wachtwoord:</span> <?php print $_SESSION['TMP_pass']; ?><br>
                    <span class="font-weight-bold">Code:</span> <?php print $_SESSION['TMP_code']; ?><br>
                    <span class="font-weight-bold text-danger">DEZE GEGEVENS ZIJN EENMALIG ZICHTBAAR</span></p>
                </div>
            </div>
        </div>
    </div>
    <?php unset($_SESSION['TMP_username']);unset($_SESSION['TMP_pass']);unset($_SESSION['TMP_code']);?>
<?php }elseif(isset($request[2]) && $request[2] == "user_deleted") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-success text-white shadow mb-3">
                <div class="card-body">
                    De geselecteerde gebruiker is succesvol verwijderd
                </div>
            </div>
        </div>
    </div>
<?php } elseif (isset($request[2]) && $request[2] == "name_duplicate") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    Er bestaat al een gebruiker met deze naam
                </div>
            </div>
        </div>
    </div>
<?php } elseif (isset($request[2]) && $request[2] == "name_empty") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    Niet alle velden waren juist ingevuld
                </div>
            </div>
        </div>
    </div>
<?php } elseif (isset($request[2]) && $request[2] == "user_delete_error") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    De geselecteerde gebruiker kan niet worden verwijderd
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php $users = getAllUsers();?>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="font-weight-bold text-primary">Gebruikers</h6>
        </div>
        <div class="card-body">
            <?php if(!empty($users)){ ?>
                <div class="table">
                            <table class="table table-bordered" id="adminOrgTable">
                                <thead>
                                <tr>
                                    <th style="width: 10%">ID</th>
                                    <th style="width: 20%">Username</th>
                                    <th style="width: 30%">Naam</th>
                                    <th style="width: 10%">Bedrijven</th>
                                    <th style="width: 10%">Admin</th>
                                    <th style="width: 20%">Actief</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($users as $user){ ?>
                                <tr>
                                    <td <?php  if($user['admin'] == 1){ print 'bgcolor="#ff8080"'; }elseif($user['active'] == 0){ print 'bgcolor="#ffe6e6"'; } ?>><?php print $user['id']; ?></td>
                                    <td <?php if($user['admin'] == 1){ print 'bgcolor="#ff8080"'; }elseif($user['active'] == 0){ print 'bgcolor="#ffe6e6"'; } ?>><?php print $user['username']; ?></td>
                                    <td <?php if($user['admin'] == 1){ print 'bgcolor="#ff8080"'; }elseif($user['active'] == 0){ print 'bgcolor="#ffe6e6"'; } ?>><?php print $user['name']; ?></td>
                                    <td <?php if($user['admin'] == 1){ print 'bgcolor="#ff8080"'; }elseif($user['active'] == 0){ print 'bgcolor="#ffe6e6"'; } ?>><?php print getAmmountOfUserOrgs($user['id']);  ?></td>
                                    <td <?php if($user['admin'] == 1){ print 'bgcolor="#ff8080"'; }elseif($user['active'] == 0){ print 'bgcolor="#ffe6e6"'; } ?>><?php if($user['admin'] == 1){print "Ja";}else{print "Nee";} ?></td>
                                    <td <?php if($user['admin'] == 1){ print 'bgcolor="#ff8080"'; }elseif($user['active'] == 0){ print 'bgcolor="#ffe6e6"'; } ?>><?php if($user['active'] == 1){print "Ja";}else{print "Nee";} ?></td>
                                    <td <?php if($user['admin'] == 1){ print 'bgcolor="#ff8080"'; }elseif($user['active'] == 0){ print 'bgcolor="#ffe6e6"'; } ?>><a href="/admin/users/<?php print $user['id']; ?>/" class="btn btn-sm btn-primary shadow-sm"><?php if($user['active'] == 1){ ?><i class="fas fa-edit fa-lg text-white"></i><?php } else { ?> <i class="fas fa-search fa-lg text-white"></i> <?php } ?></a></td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
            <?php }else{ ?>
                <div class="card bg-danger text-white shadow">
                    <div class="card-body">
                        De gebruikers kunnen momenteel niet worden geladen
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>


