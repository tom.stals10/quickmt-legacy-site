<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php $org = getOrganisation($_SESSION['org'])?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="font-weight-bold text-primary">Globale Doelen</h6>
    </div>
    <div class="card-body">
        <?php if(!empty($org)){ ?>
            <form method="POST" action="" class="user" name="adminRemoveVoorraad" id="adminRemoveVoorraad">
                <div class="form-group">
                    <input autocomplete="off" type="text" class="form-control form-control-user" name="updateDoel_Global" id="updateDoel_Global" placeholder="Doel" required><br>
                    <button type="submit" class="btn btn-primary btn-user btn-block" name="updateDoel" value="<?php print $org['id']; ?>">Aanpassen</button>
                </div>
            </form>
        <?php }else{ ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    De organisatie kan momenteel niet worden geladen
                </div>
            </div>
        <?php } ?>
    </div>
</div>