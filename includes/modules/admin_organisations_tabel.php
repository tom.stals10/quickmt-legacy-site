<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?>

<?php if(isset($request[2]) && $request[2] == "org_error") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    <?php print getLanguages("admin_organisations_tabel", "org_create_error"); ?>
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($request[2]) && $request[2] == "org_delete_error") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    <?php print getLanguages("admin_organisations_tabel", "org_delete_error"); ?>
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($request[2]) && $request[2] == "org_succes") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-success text-white shadow mb-3">
                <div class="card-body">
                    <?php print getLanguages("admin_organisations_tabel", "org_created"); ?>
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($request[2]) && $request[2] == "org_deleted") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-success text-white shadow mb-3">
                <div class="card-body">
                    <?php print getLanguages("admin_organisations_tabel", "org_deleted"); ?>
                </div>
            </div>
        </div>
    </div>
<?php } elseif (isset($request[2]) && $request[2] == "org_duplicate") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    <?php print getLanguages("admin_organisations_tabel", "org_duplicate"); ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php $orgs = getAllOrganisations();?>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="font-weight-bold text-primary"><?php print getLanguages("admin_organisations_tabel", "org_info"); ?></h6>
        </div>
        <div class="card-body">
            <?php if(!empty($orgs) >= 1){ ?>
                <div class="table">
                            <table class="table table-bordered" id="adminOrgTable">
                                <thead>
                                <tr>
                                    <th style="width: 10%"><?php print getLanguages("admin_organisations_tabel", "org_info_id"); ?></th>
                                    <th style="width: 30%"><?php print getLanguages("admin_organisations_tabel", "org_info_name"); ?></th>
                                    <th style="width: 20%"><?php print getLanguages("admin_organisations_tabel", "org_info_location"); ?></th>
                                    <th style="width: 10%"><?php print getLanguages("admin_organisations_tabel", "org_info_active"); ?></th>
                                    <th style="width: 10%"><?php print getLanguages("admin_organisations_tabel", "org_info_fantasy"); ?></th>
                                    <th style="width: 10%"><?php print getLanguages("admin_organisations_tabel", "org_info_showOpen"); ?></th>
                                    <th style="width: 10%">Partner</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($orgs as $org){ ?>
                                <tr>
                                    <td <?php  if($org['active'] == 0){ print 'bgcolor="#ff8080"'; } ?>><?php print $org['id']; ?></td>
                                    <td <?php  if($org['active'] == 0){ print 'bgcolor="#ff8080"'; } ?>><?php print $org['name']; ?></td>
                                    <td <?php  if($org['active'] == 0){ print 'bgcolor="#ff8080"'; } ?>><?php print $org['location']; ?></td>
                                    <td <?php  if($org['active'] == 0){ print 'bgcolor="#ff8080"'; } ?>><?php if($org['active'] == 1){print "Ja";}else{print "Nee";} ?></td>
                                    <td <?php  if($org['active'] == 0){ print 'bgcolor="#ff8080"'; } ?> ><?php if($org['fantasy'] == 1){print "Ja";}else{print "Nee";} ?></td>
                                    <td <?php  if($org['active'] == 0){ print 'bgcolor="#ff8080"'; } ?>><?php if($org['showOpen'] == 1){print "Ja";}else{print "Nee";} ?></td>
                                    <td <?php  if($org['active'] == 0){ print 'bgcolor="#ff8080"'; } ?>><?php if($org['partner'] == 1){print "Ja (".getOrgBelastingPercentage($org['id'])."%)";}else{print "Nee";} ?></td>
                                    <td><a href="/admin/org/<?php print $org['id']; ?>/" class="btn btn-sm btn-primary shadow-sm"><?php if($org['active'] == 1){ ?><i class="fas fa-edit fa-lg text-white"></i><?php } else { ?> <i class="fas fa-search fa-lg text-white"></i> <?php } ?></a></td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
            <?php }else{ ?>
                <div class="card bg-danger text-white shadow">
                    <div class="card-body">
                        <?php print getLanguages("admin_organisations_tabel", "org_info_cannot_load"); ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>


