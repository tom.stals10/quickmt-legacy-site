<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?>

<?php $org = getOrganisation($request[2]); ?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Partner Modus</h6>
    </div>
    <div class="card-body">
        <?php if (empty($org)){ ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    De organisatie kan niet worden geladen
                </div>
            </div>
        <?php } elseif(isset($request[2])&&$request[2]  == "1") { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    U kunt het admin account niet aanpassen
                </div>
            </div>
        <?php } elseif(!isActiveOrg($request[2])) { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    De organisatie kan niet worden aangepast aangezien deze is verwijderd
                </div>
            </div>
        <?php }else{ ?>
            <p>Wanneer partner modus is ingeschakeld wordt de organisatie niet meer verwerkt als een intern bedrijf maar als een exterene partner. Dit blokkeerd o.a. de toegang tot de sollicitaties en scoreboard</p>
            <?php if($org['partner'] != 1){ ?>
                <form method="POST" action="/includes/auth/process_partnerEnableOrg.php" class="user" name="adminOrgCreate_form" id="adminOrgCreate_form">
                    <div class="form-group">
                        <button type="submit" class="btn btn-md btn-success shadow-sm" name="org" id="org" value="<?php print $org['id']; ?>">Partner Modus Inschakelen</button>
                    </div>
                </form>
            <?php }else{ ?>
                <form method="POST" action="/includes/auth/process_partnerDisableOrg.php" class="user" name="adminOrgCreate_form" id="adminOrgCreate_form">
                    <div class="form-group">
                        <button type="submit" class="btn btn-md btn-danger shadow-sm" name="org" id="org" value="<?php print $org['id']; ?>">Partner Modus Uitschakelen</button>
                    </div>
                </form>
            <?php } ?>
        <?php } ?>
    </div>
</div>