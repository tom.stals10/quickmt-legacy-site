<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="font-weight-bold text-primary">Mededeling Versturen</h6>
    </div>
    <div class="card-body">
        <?php if(isset($_SESSION['mededelingen_verstuurd']) && $_SESSION['mededelingen_verstuurd'] == 'true'){ unset($_SESSION['mededelingen_verstuurd']); unset($_POST); ?>
            <div class="card bg-success text-white shadow">
                <div class="card-body">
                    De mededeling is zojuist verstuurd
                </div>
            </div>
            <br>
        <?php }elseif(isset($_SESSION['mededelingen_verstuurd']) && $_SESSION['mededelingen_verstuurd'] == 'false'){ unset($_SESSION['mededelingen_verstuurd']); unset($_POST); ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    De mededeling kon niet worden verstuurd
                </div>
            </div>
            <br>
        <?php }?>
        <div class="card text-white shadow">
            <div class="card-body">
                <form method="POST" action="" class="user">
                    <div class="form-group">
                        <label class="text-dark" >Gebruikers</label>
                        <select required class="selectpicker form-control" name="users[]" multiple data-live-search="true" data-selected-text-format="count" data-actions-box="true">
                            <?php foreach (getAllUsersFromOrg($_SESSION['org']) as $user){ ?>
                                <?php if(isAdmin($user['id'])) continue; ?>
                                <?php if($user['active'] == 0) continue; ?>
                                <?php if($user['id'] == $_SESSION['user_id']) continue; ?>
                                <option value="<?php print $user['id']; ?>"><?php print $user['name']?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="text-dark" >Prioriteit</label>
                        <select class="form-control" name="prioriteit">
                            <option value="urgent">Urgent</option>
                            <option value="high">High</option>
                            <option value="medium">Medium</option>
                            <option value="low" selected="selected">Low</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="text-dark" >Icon</label>
                        <select class="form-control" name="icon">
                            <option value="fa-dollar-sign">Dollar</option>
                            <option value="fa-shield-alt">Shield</option>
                            <option value="fa-box-open" selected="selected">Box</option>
                            <option value="fa-frown-open">Sadd Face</option>
                            <option value="fa-info-circle">Info</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="text-dark" >Omschrijving</label>
                        <input autocomplete="off" type="text" class="form-control" name="omschrijving" id="omschrijving" placeholder="Omschrijving" required>
                    </div>
                    <div class="form-check">
                        <input autocomplete="off" type="checkbox" class="form-check-input" id="kopieToAuthor" name="kopieToAuthor">
                        <label class="form-check-label text-dark" for="kopieToAuthor">Kopie naar autheur verzenden</label>
                    </div>
                    <br>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-user btn-block">Toevoegen</button>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
