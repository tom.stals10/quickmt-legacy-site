<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php $mutaties = getMutatieHandler($_SESSION['org'], "in");?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="font-weight-bold text-primary">Bonus toevoegen</h6>
    </div>
    <div class="card-body">
        <?php if(isset($_SESSION['bonus_added']) && $_SESSION['bonus_added'] == 'true'){ unset($_SESSION['bonus_added']); unset($_POST); ?>
            <div class="card bg-success text-white shadow">
                <div class="card-body">
                    De bonus is succesvol toegevoegd
                </div>
            </div>
            <br>
        <?php }elseif(isset($_SESSION['bonus_added']) && $_SESSION['bonus_added'] == 'false'){ unset($_SESSION['bonus_added']); unset($_POST); ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    De bonus kon niet worden toegevoegd
                </div>
            </div>
            <br>
        <?php }?>
        <?php if(hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.bonus.add")){ ?>
        <div class="card text-white shadow">
            <div class="card-body">
                <form method="POST" action="" class="user">
                    <div class="form-group">
                        <select required class="form-control" name="user">
                            <?php foreach (getAllUsers() as $user){ ?>
                                <?php if(isAdmin($user['id'])) continue; ?>
                                <?php if($user['active'] == 0) continue; ?>
                                <?php if (hasAccess($_SESSION['org'], $user['id'])) { ?>
                                    <option value="<?php print $user['id']; ?>"><?php print $user['name']; ?> (<?php print $user['username']; ?>)</option>
                                <?php } ?>
                            <?php } ?>
                        </select><br>
                        <input autocomplete="off" type="text" class="form-control form-control-user" name="omschrijving" id="omschrijving" placeholder="Omschrijving" required><br>
                        <input autocomplete="off" type="text" class="form-control form-control-user" name="bedrag" id="bedrag" placeholder="Bedrag" required><br>
                        <button type="submit" class="btn btn-success btn-user btn-block" name="type" value="add">Toevoegen</button>
                    </div>
                </form>
            </div>
        </div>
        <hr>
        <?php }else{ ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    Jij hebt geen rechten om een bonus toe te voegen
                </div>
            </div>
        <?php } ?>
    </div>
</div>