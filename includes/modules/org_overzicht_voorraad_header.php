<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Voorraad</h1>
    <?php if(hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.overzicht.voorraad")){ ?>
        <?php if(voorraadIsChecked($_SESSION['org'])){ ?>
            <a class="btn btn-md btn-primary shadow-sm disabled text-white">De voorraad is vandaag al gecontroleerd door <?php print voorraadCheckedBy($_SESSION['org'])?></a>
        <?php }else{ ?>
            <a href="/org/overzicht/voorraad/checked/" class="btn btn-md btn-primary shadow-sm text-white">Ik heb de voorraad gecontroleerd</a>
        <?php } ?>
    <?php } ?>
</div>
<p class="font-weight-bold"><span class="text-danger">Als de voorraad wordt gewijzigd kan dit niet worden teruggedraaid</span></p>
