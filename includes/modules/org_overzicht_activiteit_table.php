<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

if(isset( $_SESSION['activity_start'],$_SESSION['activity_end'])) {
    $startDay = $_SESSION['activity_start'];
    $endDay =$_SESSION['activity_end'];

    $time = $startDay + 21600;

    $users = getAllUsersFromOrg($_SESSION['org']); ?>



<div class="card shadow mb-4">
    <!-- Card Header - Accordion -->
    <a href="#activiteit_1" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="activiteit_1">
        <h6 class="m-0 font-weight-bold text-primary">Activiteit (Ochtend) | <?php print date('d-m-Y', $_SESSION['activity_start'])?></h6>
    </a>
    <!-- Card Content - Collapse -->
    <div class="collapse hide" id="activiteit_1">
        <div class="card-body">


            <div class="row">
                <div class="table">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th style="width: 10%"></th>
                            <?php foreach ($users as $user){ ?>
                                <?php if(isAdmin($user['id'])) continue; ?>
                                <?php if($user['active'] == 0) continue; ?>
                                <th><?php print $user['name'] ?></th>
                            <?php }?>
                        </tr>
                        </thead>
                        <tbody>

                        <?php while($time < ($endDay - 7199) - 36000){ ?>
                            <tr>
                                <td><?php print date('H:i', $time)?> - <?php print date('H:i', $time + 899)?></td>
                                <?php foreach ($users as $user){ ?>
                                    <?php if(isAdmin($user['id'])) continue; ?>
                                    <?php if($user['active'] == 0) continue; ?>
                                    <?php if(isInActive($_SESSION['org'], $user['id'], $time)){ ?>
                                        <td style="background-color: red"></td>
                                    <?php }elseif(userWasActive($_SESSION['org'], $user['id'],$time-1,$time+900)){ ?>
                                        <td style="background-color: green"></td>
                                    <?php }else{ ?>
                                        <td></td>
                                    <?php } ?>
                                <?php }?>
                            </tr>
                            <?php $time += 900; ?>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#activiteit_2" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="activiteit_2">
            <h6 class="m-0 font-weight-bold text-primary">Activiteit (Middag) | <?php print date('d-m-Y', $_SESSION['activity_start'])?></h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse hide" id="activiteit_2">
            <div class="card-body">


                <div class="row">
                    <div class="table">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 10%"></th>
                                <?php foreach ($users as $user){ ?>
                                    <?php if(isAdmin($user['id'])) continue; ?>
                                    <?php if($user['active'] == 0) continue; ?>
                                    <th><?php print $user['name'] ?></th>
                                <?php }?>
                            </tr>
                            </thead>
                            <tbody>

                            <?php while($time < ($endDay - 7199) - 21600){ ?>
                                <tr>
                                    <td><?php print date('H:i', $time)?> - <?php print date('H:i', $time + 899)?></td>
                                    <?php foreach ($users as $user){ ?>
                                        <?php if(isAdmin($user['id'])) continue; ?>
                                        <?php if($user['active'] == 0) continue; ?>
                                        <?php if(isInActive($_SESSION['org'], $user['id'], $time)){ ?>
                                            <td style="background-color: red"></td>
                                        <?php }elseif(userWasActive($_SESSION['org'], $user['id'],$time-1,$time+900)){ ?>
                                            <td style="background-color: green"></td>
                                        <?php }else{ ?>
                                            <td></td>
                                        <?php } ?>
                                    <?php }?>
                                </tr>
                                <?php $time += 900; ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#activiteit_3" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="activiteit_3">
            <h6 class="m-0 font-weight-bold text-primary">Activiteit (Avond) | <?php print date('d-m-Y', $_SESSION['activity_start'])?></h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse hide" id="activiteit_3">
            <div class="card-body">


                <div class="row">
                    <div class="table">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 10%"></th>
                                <?php foreach ($users as $user){ ?>
                                    <?php if(isAdmin($user['id'])) continue; ?>
                                    <?php if($user['active'] == 0) continue; ?>
                                    <th><?php print $user['name'] ?></th>
                                <?php }?>
                            </tr>
                            </thead>
                            <tbody>

                            <?php while($time < ($endDay - 7199)){ ?>
                                <tr>
                                    <td><?php print date('H:i', $time)?> - <?php print date('H:i', $time + 899)?></td>
                                    <?php foreach ($users as $user){ ?>
                                        <?php if(isAdmin($user['id'])) continue; ?>
                                        <?php if($user['active'] == 0) continue; ?>
                                        <?php if(isInActive($_SESSION['org'], $user['id'], $time)){ ?>
                                            <td style="background-color: red"></td>
                                        <?php }elseif(userWasActive($_SESSION['org'], $user['id'],$time-1,$time+900)){ ?>
                                            <td style="background-color: green"></td>
                                        <?php }else{ ?>
                                            <td></td>
                                        <?php } ?>
                                    <?php }?>
                                </tr>
                                <?php $time += 900; ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
}
?>
