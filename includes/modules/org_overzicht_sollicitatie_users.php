<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><hr>
<?php if(isset($_SESSION['sollicitatie_handled']) && $_SESSION['sollicitatie_handled'] == 'false'){ unset($_SESSION['sollicitatie_handled']); unset($_POST); ?>
    <div class="card bg-success text-white shadow">
        <div class="card-body">
            De sollicitatie is succesvol behandeld
        </div>
    </div>
    <br>
<?php }?>
<?php if(isset($_SESSION['sollicitatie_handled']) && $_SESSION['sollicitatie_handled'] == 'false'){ unset($_SESSION['sollicitatie_handled']); unset($_POST); ?>
    <div class="card bg-warning text-white shadow">
        <div class="card-body">
            De sollicitatie kon niet worden verwerkt
        </div>
    </div>
    <br>
<?php } ?>
<?php $sollicitaties = getSollicitaties()?>
<div class="card shadow mb-4">
    <a href="#sollicitatie_open" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sollicitatie_open">
        <h6 class="m-0 font-weight-bold text-primary">Openstaande Sollicitaties</h6>
    </a>
    <div class="collapse hide" id="sollicitatie_open">
        <div class="card-body">
            <?php
            foreach ($sollicitaties as $sollicitatie){
                if($sollicitatie['status'] != 'pending') continue;
                printSollicitatie($sollicitatie, true);
            }
            ?>
        </div>
    </div>
</div>
<div class="card shadow mb-4">
    <a href="#sollicitatie_accepted" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sollicitatie_accepted">
        <h6 class="m-0 font-weight-bold text-primary">Archief</h6>
    </a>
    <div class="collapse hide" id="sollicitatie_accepted">
        <div class="card-body">
            <?php
            foreach ($sollicitaties as $sollicitatie){
                if($sollicitatie['status'] != 'archive') continue;
                printSollicitatie($sollicitatie, false);
            }
            ?>
        </div>
    </div>
</div>


