<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

include_once 'psl-config.php';
require_once __DIR__.'/../../includes/php/meldingHandler.php';
require_once __DIR__.'/../../includes/php/userInfo.php';

function sec_session_start() {
    $session_name = 'QuickMT';   // Set a custom session name
    $secure = true;
    $httponly = true;
    ini_set('session.use_only_cookies', 1);
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"],
        $cookieParams["path"],
        $cookieParams["domain"],
        $secure,
        $httponly);
    session_name($session_name);
    ini_set('session.use_strict_mode', 0);
    session_start();
    session_regenerate_id();
}

function login($username, $password, $code, $mysqli) {
    if ($stmt = $mysqli->prepare("SELECT id, password FROM user WHERE username = ? AND code = ? LIMIT 1")) {
        $stmt->bind_param('si', $username, $code);
        $stmt->execute();
        $stmt->store_result();

        $stmt->bind_result($user_id, $db_password);
        $stmt->fetch();

        if ($stmt->num_rows == 1) {
            if (checkbrute($user_id, $mysqli) == true) {
                return false;
            } else {
                if (password_verify($password, $db_password)) {
                    $user_browser = $_SERVER['HTTP_USER_AGENT'];
                    $user_id = preg_replace("/[^0-9]+/", "", $user_id);
                    $_SESSION['user_id'] = $user_id;
                    $username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username);
                    $_SESSION['username'] = $username;
                    $_SESSION['login_string'] = hash('sha512', $db_password . $user_browser);
                    return true;
                } else {
                    $now = time();
                    $ip = getUserIp();
                    $mysqli->query("INSERT INTO login_attempts_user(user_id, time) VALUES ('$user_id', '$now')");
                    return false;
                }
            }
        } else {
            $now = time();
            $ip = getUserIp();
            $mysqli->query("INSERT INTO login_attempts_ip(ip, time) VALUES ('$ip', '$now')");
            return false;
        }
    }
}

function checkbrute($user_id, $mysqli) {
    $now = time();
    $ip = getUserIp();

    $valid_attempts = $now - (5 * 60);

    if ($stmt = $mysqli->prepare("SELECT time FROM login_attempts_user WHERE user_id = ?  AND time > '$valid_attempts'")) {
        $stmt->bind_param('i', $user_id);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows > 3){
            $meldigID = 'InvalidLogin::userid::'.rand(111111111,999999999);
            createMelding_with_mysqli($mysqli,$user_id,0,'urgent', 'fa-shield-alt', 'Er is 3 keer geprobeerd in te loggen in jou account met een onjuist wachtwoord', $meldigID);
            $users = getAllUsers();
            foreach ($users as $user){
                if(isAdmin($user['is'])){
                    createMelding_with_mysqli($mysqli,$user['id'],0,'urgent', 'fa-shield-alt', 'Er is 3 keer geprobeerd in te loggen met een onjuist wachtwoord in het account: '.getUserInfo($user_id)['name'] . ' ('.$user['username'].')', $meldigID);
                }
            }
        }
        if ($stmt->num_rows > 5) {
            return true;
        }
    }

    if ($stmt = $mysqli->prepare("SELECT time FROM login_attempts_ip WHERE ip = ?  AND time > '$valid_attempts'")) {
        $stmt->bind_param('s', $ip);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows > 3){
            $meldigID = 'InvalidLogin::IP::'.rand(111111111,999999999);
            $users = getAllUsers();
            foreach ($users as $user){
                if(isAdmin($user['is'])){
                    createMelding_with_mysqli($mysqli,$user['id'],0,'urgent', 'fa-shield-alt', 'Er is 3 keer geprobeerd in te loggen met een onjuist account vanaf het IP: '. $ip, $meldigID);
                }
            }
        }
        if ($stmt->num_rows > 5) {
            return true;
        }
    }
}

function login_check($mysqli) {
    if (isset($_SESSION['user_id'],$_SESSION['username'],$_SESSION['login_string'])) {
        $user_id = $_SESSION['user_id'];
        $login_string = $_SESSION['login_string'];
        $username = $_SESSION['username'];

        if(!isActiveUserByName($username, $mysqli)){
            return false;
        }

        $user_browser = $_SERVER['HTTP_USER_AGENT'];
        if ($stmt = $mysqli->prepare("SELECT password FROM user WHERE id = ? LIMIT 1")) {
            $stmt->bind_param('i', $user_id);
            $stmt->execute();
            $stmt->store_result();
            if ($stmt->num_rows == 1) {
                $stmt->bind_result($password);
                $stmt->fetch();
                $login_check = hash('sha512', $password . $user_browser);
                if (hash_equals($login_check, $login_string) ){
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function login_checkWithVariables($mysqli, $user_id, $username, $login_string) {
    if(!isActiveUserByName($username, $mysqli)){
        return false;
    }
    $user_browser = $_SERVER['HTTP_USER_AGENT'];
    if ($stmt = $mysqli->prepare("SELECT password FROM user WHERE id = ? LIMIT 1")) {
        $stmt->bind_param('i', $user_id);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt->bind_result($password);
            $stmt->fetch();
            $login_check = hash('sha512', $password . $user_browser);
            if (hash_equals($login_check, $login_string) ){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function esc_url($url) {
    if ('' == $url) {
        return $url;
    }
    $url = preg_replace('|[^a-z0-9-~+_.?#=!&;,/:%@$\|*\'()\x80-\xff]|i', '', $url);
    $strip = array('%0d', '%0a', '%0D', '%0A');
    $url = (string) $url;
    $count = 1;
    while ($count) {
        $url = str_replace($strip, '', $url, $count);
    }
    $url = str_replace(';//', '://', $url);
    $url = htmlentities($url);
    $url = str_replace('&amp;', '&', $url);
    $url = str_replace("'", '\'', $url);
    if ($url[0] !== '/') {
        return '';
    } else {
        return $url;
    }
}

function getUserIp(){
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function validatePassword($userid, $pass){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT password FROM user WHERE id = ? LIMIT 1")) {
        $stmt->bind_param('i', $userid);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt->bind_result($db_password);
            $stmt->fetch();
            if (password_verify($pass, $db_password) ){
                return true;
            }
        }
    }
    return false;
}

function changePassword($userid, $pass){
    global $mysqli;
    $pass = password_hash($pass, PASSWORD_BCRYPT);
    $stmt = $mysqli->prepare("UPDATE user SET password = ? WHERE id = ?");
    $stmt->bind_param('si', $pass,$userid);
    $stmt->execute();
    $now = time();
    $stmt = $mysqli->prepare("UPDATE userStats SET lastPasswordUpdate = ? WHERE user_id = ?");
    $stmt->bind_param('si', $now, $userid);
    $stmt->execute();
}

function isActiveUserByName($username, $mysqli){
    $active = 1;
    if ($stmt = $mysqli->prepare("SELECT active FROM user WHERE active = ? AND username = ? LIMIT 1")) {
        $stmt->bind_param('is', $active , $username);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            return true;
        }
    }
    return false;
}