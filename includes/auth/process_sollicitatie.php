<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

ob_start();
include '../../errorHandler.php';
register_shutdown_function('shutdownErrorFunction', $_SESSION);

include_once 'db_connect.php';
include_once 'functions.php';
include_once '../php/meldingHandler.php';
include_once '../php/userInfo.php';
include_once '../php/orgHandler.php';
include_once '../php/sollicitatieHandler.php';
include_once '../php/meldingHandler.php';
include_once '../php/permHandler.php';

if(isset($_POST['functie'],$_POST['username'],$_POST['name'],$_POST['tutorial'],$_POST['online_time'],$_POST['start_date'],$_POST['online_time_day'],$_POST['online_time_day_shop'],$_POST['level'],$_POST['city'],$_POST['leeftijd'],$_POST['contact'],$_POST['vog'],$_POST['job'], $_POST['werkervaring'],$_POST['motivatie'],$_POST['contact_info'])){
    $functie = ltrim(rtrim(strip_tags($_POST['functie'])));
    $username = ltrim(rtrim(strip_tags($_POST['username'])));
    $name = ltrim(rtrim(strip_tags($_POST['name'])));
    $tutorial = ltrim(rtrim(strip_tags($_POST['tutorial'])));
    $online_time = ltrim(rtrim(strip_tags($_POST['online_time'])));
    $start_date = ltrim(rtrim(strip_tags($_POST['start_date'])));
    $online_time_day = ltrim(rtrim(strip_tags($_POST['online_time_day'])));
    $online_time_day_shop = ltrim(rtrim(strip_tags($_POST['online_time_day_shop'])));
    $level = ltrim(rtrim(strip_tags($_POST['level'])));
    $city = ltrim(rtrim(strip_tags($_POST['city'])));
    $leeftijd = ltrim(rtrim(strip_tags($_POST['leeftijd'])));
    $contact = ltrim(rtrim(strip_tags($_POST['contact'])));
    $vog = ltrim(rtrim(strip_tags($_POST['vog'])));
    $job = ltrim(rtrim(strip_tags($_POST['job'])));
    $vog = ltrim(rtrim(strip_tags($_POST['vog'])));
    $werkervaring = ltrim(rtrim(strip_tags($_POST['werkervaring'])));
    $motivatie = ltrim(rtrim(strip_tags($_POST['motivatie'])));
    $contact_info = ltrim(rtrim(strip_tags($_POST['contact_info'])));
    if(isset($_POST['job_text'])){
        $job_text = ltrim(rtrim(strip_tags($_POST['job_text'])));
    }else{
        $job_text = '-';
    }
    if(isset($_POST['comp'])){
        $comp = ltrim(rtrim(strip_tags($_POST['comp'])));
    }else{
        $comp = '-';
    }
    addSollicitatie($functie,$username,$name,$tutorial,$online_time,$start_date,$online_time_day,$online_time_day_shop,$level,$city,$leeftijd,$contact,$job,$vog,$werkervaring,$motivatie,$contact_info,$job_text,$comp);
    $usersIDs = array();
    $groupMeldingID = 'SollicitatieMelding::'.rand(111111111,999999999);
    foreach (getAllOrganisations() as $org){
        if(isPartner($org['id'])) continue;
        foreach (getAllUsersFromOrg($org['id']) as $user){
            if(hasPerms($org['id'], $user['id'], 'page.overzicht.sollicitatie') && !isAdmin($user['id'])){
                if(!in_array($user['id'], $usersIDs)){
                    $usersIDs[] = $user['id'];
                    createMelding($user['id'], 0, 'medium', 'sticky-note', 'Er is zojuist een nieuwe sollicitatie ingediend!',$groupMeldingID);
                }
            }
        }
    }
}
header('Location: /solliciteren/-/');
exit();