<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

ob_start();
include '../../errorHandler.php';
register_shutdown_function('shutdownErrorFunction', $_SESSION);

include_once 'db_connect.php';
include_once 'functions.php';
include_once '../php/logHandler.php';

sec_session_start();

if (isset($_POST['username'], $_POST['p'], $_POST['medwcode'])) {
    $username = $_POST['username'];
    $password = $_POST['p'];
    $code = $_POST['medwcode'];

    if (login($username, $password, $code, $mysqli) == true) {
        if(!isActiveUserByName($username, $mysqli)){
            unset($_SESSION['user_id']);
            unset($_SESSION['username']);
            unset($_SESSION['login_string']);
            header('Location: /inactive/');
            exit();
        }
        addLog($_SESSION['user_id'], "Succesvolle Inlogpoging");
        header('Location: /');
        exit();
    } else {
        header('Location: /error/');
        exit();
    }
} else {
    header('Location: /error/');
    exit();
}