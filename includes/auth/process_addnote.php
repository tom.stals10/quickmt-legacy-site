<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

ob_start();
include '../../errorHandler.php';
register_shutdown_function('shutdownErrorFunction', $_SESSION);



include_once 'db_connect.php';
include_once 'functions.php';
include_once '../php/logHandler.php';
include_once '../php/orgHandler.php';
include_once '../php/userInfo.php';
include_once '../php/permHandler.php';
include_once '../php/noteHandler.php';

sec_session_start();

if(login_check($mysqli) != true) {
    header('Location: /error/');
    exit();
}

if(isset($_POST['titel'], $_POST['bericht'])){
    $titel = ltrim(rtrim(strip_tags($_POST['titel'])));
    $bericht = strip_tags($_POST['bericht']);

    if($titel == "" || empty($titel)){
        header('Location: /org/notities/empty/');
        exit();
    }

    if($bericht == "" || empty($bericht)){
        header('Location: /org/notities/empty/');
        exit();
    }

    if(!hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.notities.add")){
        header('Location: /org/notities/no_perms/');
        exit();
    }

    if(!hasAccess($_SESSION['org'], $_SESSION['user_id'])){
        header('Location: /');
        exit();
    }

    addNotitie($_SESSION['org'], $_SESSION['user_id'],$titel, $bericht);

    $user = getUserInfo($_SESSION['user_id']);
    $org = getOrganisation($_SESSION['org']);

    addLog($_SESSION['user_id'], "Succesvolle een notitie toegevoegd voor de organisatie ". $org['name']. " (".$org['location'].") Onderwerp: " . $titel);

    header('Location: /org/notities/success/');
    exit();
}
header('Location: /org/notities/');
exit();