<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php require_once './includes/modules/org_overzicht_bonus_header.php';?>
<?php if (isset($_POST['omschrijving'],$_POST['bedrag'],$_POST['type'],$_POST['user'])) {
    $omschrijving = $_POST['omschrijving'];
    $omschrijving = strip_tags($omschrijving);
    $bedrag = $_POST['bedrag'];
    $bedrag = str_replace(",", ".", $bedrag);
    $type = $_POST['type'];
    $user = $_POST['user'];
    if($type == 'add' && userExistsByUserID($user) && hasAccess($_SESSION['org'], $user)){
        if(hasPerms($_SESSION['org'], $_SESSION['user_id'], 'page.overzicht.bonus.add') && !empty($omschrijving) && is_numeric($bedrag)){
            $bedrag = round($bedrag,1);
            addBonus($_SESSION['org'], $user, $omschrijving, $bedrag);
            createMelding($user,$_SESSION['user_id'],'low', 'fa-dollar-sign', 'Je hebt voor de huidige periode een bonus ontvangen van €'.$bedrag .' met als omschrijving: ' . $omschrijving, "BonusOntvangen::".rand(111111111,999999999));
            $org = getOrganisation($_SESSION['org']);
            $user = getUserInfo($user);
            addLog($_SESSION['user_id'], "Succesvolle voor de organisatie ". $org["name"] . " (".$org['location'].") voor de gebruiker ".$user['username']." (".$user['name'].") een bonus toegevoegd ter grootte van: " . $bedrag);
            $_SESSION['bonus_added'] = 'true';
        }
    }
    if(!isset($_SESSION['bonus_added'])){
        $_SESSION['bonus_added'] = 'false';
    }
} ?>
<?php if (isset($request[3],$request[4]) && $request[3] == 'delete') {
    $bonusID = $request[4];
    if(hasPerms($_SESSION['org'], $_SESSION['user_id'], 'page.overzicht.bonus.remove') && is_numeric($bonusID) && bonusExists($bonusID) && !isAdmin($_SESSION['user_id'])){
        $bonus = getBonus($bonusID);
        if($bonus['time'] > getOrganisation($_SESSION['org'])['lastLoonReset'] ){
            $org = getOrganisation($_SESSION['org']);
            $user = getUserInfo($bonus['user_id']);
            addLog($_SESSION['user_id'], "Succesvolle voor de organisatie ". $org["name"] . " (".$org['location'].") voor de gebruiker ".$user['username']." (".$user['name'].") een bonus verwijderd ter grootte van: " . $bonus['total']);
            removeBonus($bonusID);
            $_SESSION['bonus_removed'] = 'true';
        }

    }
    if(!isset($_SESSION['bonus_removed'])){
        $_SESSION['bonus_removed'] = 'false';
    }
} ?>
<div class="row">
    <div class="col-xl-6 col-lg-6">
        <?php require_once './includes/modules/org_overzicht_bonus_add.php'; ?>
    </div>
    <div class="col-xl-6 col-lg-6">
        <?php require_once './includes/modules/org_overzicht_bonus_list.php'; ?>
    </div>
</div>

