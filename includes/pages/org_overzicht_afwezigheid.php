<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php require_once './includes/modules/org_overzicht_activiteit_header.php'; ?>

<?php if (isset($request[3],$request[4])) {
    $afwezigheidsID = $request[3];
    $updateStatus = $request[4];
    if($updateStatus == "accept" && afwezigheidsMeldingExists($afwezigheidsID) && getAfwezigheid($afwezigheidsID)[0]['status'] == "in behandeling" && hasPerms($_SESSION['org'], $_SESSION['user_id'], 'page.overzicht.afwezigheid')){
        updateAfwezigheidStatus($afwezigheidsID, "geaccepteerd", $_SESSION['user_id']);
        addLog($_SESSION['user_id'], "Succesvolle de afwezigheid met id " . $afwezigheidsID. " behandeld (status: geaccepteerd).");
        $_SESSION['afwezigheid_handled'] = 'true';
    }
    if($updateStatus == "denied" && afwezigheidsMeldingExists($afwezigheidsID) && getAfwezigheid($afwezigheidsID)[0]['status'] == "in behandeling" && hasPerms($_SESSION['org'], $_SESSION['user_id'], 'page.overzicht.afwezigheid')){
        updateAfwezigheidStatus($afwezigheidsID, "geweigerd", $_SESSION['user_id']);
        addLog($_SESSION['user_id'], "Succesvolle de afwezigheid met id " . $afwezigheidsID. " behandeld (status: geweigerd).");
        $_SESSION['afwezigheid_handled'] = 'false';
    }

    if(!isset($_SESSION['afwezigheid_handled'])){
        $_SESSION['afwezigheid_handled'] = 'null';
    }
    echo '<script>window.location.href = "/org/overzicht/afwezigheid/";</script>';
} ?>

<?php require_once './includes/modules/org_overzicht_afwezigheid_list.php'; ?>