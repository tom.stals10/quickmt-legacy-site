<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php require_once './includes/modules/org_overzicht_activiteit_header.php'; ?>
<form action="" method="POST">
    <div class="form-group">
        <input autocomplete="off" class="form-control" id="date" name="date" placeholder="dd-mm-yyyy" type="text"/> <br>
        <button type="submit" class="btn btn-primary btn-user btn-block">Doorgaan</button>
    </div>
</form>
    <script>
        $(document).ready(function(){
            var date_input=$('input[name="date"]'); //our date input has the name "date"
            var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
            var options={
                format: 'dd-mm-yyyy',
                startDate: '-29d',
                endDate: 'today',
                container: container,
                todayHighlight: true,
                autoclose: true,
            };
            date_input.datepicker(options);
        })
    </script>

<?php if(isset($_POST['date'])){
    $date = strtotime($_POST['date']);
    $startDay = $date;
    $endDay = $startDay + (60*60*24) - 1;
    if($startDay < strtotime('now') && $endDay > strtotime('-29 days')){
        $_SESSION['activity_start'] = $startDay;
        $_SESSION['activity_end'] = $endDay;
        require_once './includes/modules/org_overzicht_activiteit_table.php';
    }
}