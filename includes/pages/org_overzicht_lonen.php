<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

if(isset($request[3]) && $request[3] = "calculate"){
    if(hasPerms($_SESSION['org'],$_SESSION['user_id'], "page.overzicht.lonen.berekenen") && !partnerHasOpenPayment($_SESSION['org'])){
        $now = time();
        if((getOrganisation($_SESSION['org'])['lastLoonReset'] == 0 || getOrganisation($_SESSION['org'])['lastLoonReset'] < $now - (60*60*24))){
            $users = getAllUsersFromOrg($_SESSION['org']);
            $totaleLoon = 0;
            $meldingID = 'LoonBerekening::' . rand(111111111,999999999);
            if(!empty($users)){
                foreach ($users as $user){
                    $userBonus = getBonussen_Bedrag($_SESSION['org'], $user['id'], getOrganisation($_SESSION['org'])['lastLoonReset']);
                    $loon = getOmzetTotalPerUserSinceLastoonRest($_SESSION['org'], $user['id'], getOrganisation($_SESSION['org'])['lastLoonReset']) * getLoonPercentage($_SESSION['org'],$user['id']);
                    $loon = round($loon, 1);
                    $loon += $userBonus['total'];
                    $loon = round($loon, 1);
                    if($loon != 0){
                        $totaleLoon += $loon;
                        addLoon($_SESSION['org'], $user['id'], $loon, $now, $_SESSION['user_id']);
                        createMelding($user['id'],$_SESSION['user_id'],'low', 'fa-dollar-sign', 'Je loon voor de huidige periode is berekend', $meldingID);
                        $org = getOrganisation($_SESSION['org']);
                        addLog($_SESSION['user_id'], "Succesvolle voor de organisatie ". $org["name"] . " (".$org['location'].") het loon berekend van de gebruiker ".getUserInfo($user['id'])['username']." (".getUserInfo($user['id'])['name']."))");
                    }
                }
            }
            $totaleLoon = round($totaleLoon, 1);
            $statsPin = getOmzetTotalSinceLastoonRest($_SESSION['org'],getOrganisation($_SESSION['org'])['lastLoonReset'], 'pin');
            $statsContant = getOmzetTotalSinceLastoonRest($_SESSION['org'],getOrganisation($_SESSION['org'])['lastLoonReset'], 'contant');
            $totaleOmzet = round($statsPin+$statsContant,1);
            $Kasin = round(getTotaleMutaties($_SESSION['org'], "in")['total'] + 0,1);
            $Kasuit = round(getTotaleMutaties($_SESSION['org'], "out")['total'] + 0,1);
            $totaleKas =round($Kasin - $Kasuit,1);
            addMutatieLog($_SESSION['org'], $totaleOmzet, $totaleKas);
            if(isPartner($_SESSION['org'])){
                $belasting = ceil(($totaleOmzet/100)*getOrgBelastingPercentage($_SESSION['org']));
                addMutatie($_SESSION['org'], 'out', $belasting, 'Contributie Quick Company - ' . date("d-m-Y H:i", time()));
                createPayment($_SESSION['org'],$belasting);
            }
            addMutatie($_SESSION['org'], 'out', $totaleLoon, 'Loonberekening - ' . date("d-m-Y H:i", time()));
            setLastLoonPayed($_SESSION['org'], $now);
            $_SESSION['lonen_calculated'] = 'true';

            foreach ($users as $user){
                if(isPendingDeleted($user['id'],$_SESSION['org'])){
                    setLoonPercentageForPartners($_SESSION['org'], $user['id'], 0);
                    setPersoonlijkeDoel($_SESSION['org'],$user['id'],0);
                    deleteUserFromOrgAfterPending($_SESSION['org'],$user['id']);
                }
            }
        }
    }

    if(!isset($_SESSION['lonen_calculated'])){
        $_SESSION['lonen_calculated'] = 'false';
    }
    echo "<script> window.location.replace('/org/overzicht/lonen/') </script>";
}
?>


<?php require_once './includes/modules/org_overzicht_lonen_header.php'; ?>
<?php require_once './includes/modules/org_overzicht_lonen_now.php'; ?>
<?php if(getOrganisation($_SESSION['org'])['lastLoonReset'] != 0){ ?>
    <?php require_once './includes/modules/org_overzicht_lonen_last.php'; ?>
    <?php require_once './includes/modules/org_overzicht_lonen_users.php'; ?>
<?php } ?>
