<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php require_once './includes/modules/org_afwezigheid_header.php'; ?>

<?php if (isset($_POST['begin'],$_POST['end'],$_POST['reden'])) {
    $begin = strtotime($_POST['begin']);
    $omschrijving = $_POST['reden'];
    $omschrijving = strip_tags($omschrijving);
    $end =  strtotime($_POST['end'])  + (60*60*24) - 1;

    $meldingID = 'AfwezigheidMelding::'.time()."::".rand(111111111,999999999);
    if(hasPerms($_SESSION['org'], $_SESSION['user_id'], 'page.afwezigheid') && !empty($omschrijving) && $end > $begin){
        createAfwezigheid($_SESSION['org'], $_SESSION['user_id'], $begin, $end, $omschrijving);
        foreach (getAllUsersFromOrg($_SESSION['org']) as $user){
            if(isAdmin($user['id'])) continue;
            if($user['active'] == 0) continue;
            if(!hasPerms($_SESSION['org'], $user['id'], 'page.overzicht.afwezigheid')) continue;
            createMelding($user['id'], 0, 'high', 'fa-exclamation-triangle',  'Er is zojuist een nieuwe afwezigheids aanvraag ingevuld.', $meldingID);
        }
        $_SESSION['afwezigheid_verstuurd'] = 'true';
    }

    if(!isset($_SESSION['afwezigheid_verstuurd'])){
        $_SESSION['afwezigheid_verstuurd'] = 'false';
    }
    echo '<script>window.location.href = "/org/afwezigheid/";</script>';
} ?>

<div class="row">
    <div class="col-xl-6 col-lg-6">
        <?php require_once './includes/modules/org_afwezigheid_sent.php'; ?>
    </div>
    <div class="col-xl-6 col-lg-6">
        <?php require_once './includes/modules/org_afwezigheid_list.php'; ?>
    </div>
</div>
