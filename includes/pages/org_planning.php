<?php require_once './includes/modules/org_planning_header.php'; ?>
    <form action="" method="POST">
        <div class="form-group">
            <input autocomplete="off" class="form-control" id="date" name="date" placeholder="dd-mm-yyyy" type="text"/> <br>
            <button type="submit" class="btn btn-primary btn-user btn-block">Doorgaan</button>
        </div>
    </form>
    <script>
        $(document).ready(function(){
            var date_input=$('input[name="date"]'); //our date input has the name "date"
            var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
            var options={
                format: 'dd-mm-yyyy',
                startDate: '-29d',
                endDate: '+29d',
                container: container,
                todayHighlight: true,
                autoclose: true,
            };
            date_input.datepicker(options);
        })
    </script>

<?php if(isset($_POST['date'])){
    $date = strtotime($_POST['date']);
    $startDay = $date;
    $endDay = $startDay + (60*60*24) - 1;
    if($startDay > strtotime('-30 days') && $endDay < strtotime('+30 days')){
        $_SESSION['planning_start'] = $startDay;
        $_SESSION['planning_end'] = $endDay;
        require_once './includes/modules/org_planning_table.php';
    }
}else if(isset($_SESSION['planning_start'],$_SESSION['planning_end'])){
    if(isset($request[2],$request[3]) && $request[3] == 'claim' && is_numeric($request[2]) && $request[2] >= 1 && $request[2] <= 448){
        if($_SESSION['planning_end'] < time()){
            echo '<script>window.location.href = "/org/planning/";</script>';
            return;
        }
        if(!isClaimedSlot($_SESSION['org'], $_SESSION['planning_start'], $request[2]) && hasPerms($_SESSION['org'],$_SESSION['user_id'], 'page.planning') && !isAdmin($_SESSION['user_id'])){
            claimPlanningSlot($_SESSION['org'], $_SESSION['planning_start'], $request[2], $_SESSION['user_id']);
            echo '<script>window.location.href = "/org/planning/";</script>';
        }else{
            $planningSlot = getPlanningSlot($_SESSION['org'], $_SESSION['planning_start'], $request[2])[0]['user_id'];
            if($planningSlot == 0 || !hasAccess($_SESSION['org'], $planningSlot)|| !hasPerms($_SESSION['org'], $planningSlot, 'page.planning') || isAdmin($planningSlot)|| !isActiveUser($planningSlot)) {
                claimPlanningSlot($_SESSION['org'], $_SESSION['planning_start'], $request[2], $_SESSION['user_id']);
                echo '<script>window.location.href = "/org/planning/";</script>';
            }elseif($planningSlot == $_SESSION['user_id']){
                unclaimPlanningSlot($_SESSION['org'], $_SESSION['planning_start'], $request[2]);
                echo '<script>window.location.href = "/org/planning/";</script>';
            }elseif(hasPerms($_SESSION['org'], $_SESSION['user_id'], 'page.planning.manage') && !isAdmin($_SESSION['user_id'])){
                unclaimPlanningSlot($_SESSION['org'], $_SESSION['planning_start'], $request[2]);
                echo '<script>window.location.href = "/org/planning/";</script>';
            }
        }
    }
    $startDay = $_SESSION['planning_start'];
    $endDay = $_SESSION['planning_end'];
    if($startDay > strtotime('-30 days') && $endDay < strtotime('+30 days')){
        require_once './includes/modules/org_planning_table.php';
    }
}
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */


