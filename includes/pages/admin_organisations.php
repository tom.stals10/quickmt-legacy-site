<?php require_once './includes/modules/admin_organisations_header.php'; ?>

<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

if(isset($request[2]) && is_numeric($request[2]) && orgExistsByID($request[2])){ ?>
    <div class="row">
        <div class="col-xl-8 col-lg-7">
            <?php require_once './includes/modules/admin_organisations_information.php'; ?>
            <?php if($request[2] != 1){ ?>
                <?php  require_once './includes/modules/admin_organisations_partner.php'; ?>
                <?php  require_once './includes/modules/admin_organisations_showOpen.php'; ?>
                <?php if(!isPartner($request[2])){ ?>
                    <?php  require_once './includes/modules/admin_organisations_fantasy.php'; ?>
                <?php } ?>
            <?php } ?>
        </div>
        <div class="col-xl-4 col-lg-5">
            <?php require_once './includes/modules/admin_organisations_name.php'; ?>
            <?php require_once './includes/modules/admin_organisations_location.php'; ?>
            <?php require_once './includes/modules/admin_organisations_icon.php'; ?>
            <?php if(isPartner($request[2])){ ?>
                <?php require_once './includes/modules/admin_organisations_belasting.php'; ?>
            <?php } ?>
        </div>
    </div>
<?php }else{
    require_once './includes/modules/admin_organisations_tabel.php';
}
?>
