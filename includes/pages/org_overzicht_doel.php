<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

if(isset($_POST['updateDoel'])){
    if(hasPerms($_SESSION['org'],$_SESSION['user_id'], "page.overzicht.doel.manage") && !isAdmin($_SESSION['user_id'])){
        $id = $_POST['updateDoel'];
        if(isset($_POST['updateDoel_User']) && is_numeric($_POST['updateDoel_User']) && $_POST['updateDoel_User'] >= 0){
            $doel = round($_POST['updateDoel_User'],1);
            if(!isAdmin($id) && hasAccess($_SESSION['org'], $id)){
                setPersoonlijkeDoel($_SESSION['org'], $id, $doel);
                $org = getOrganisation($_SESSION['org']);
                $user = getUserInfo($id);
                addLog($_SESSION['user_id'], "Succesvolle voor de organisatie ". $org["name"] . " (".$org['location'].") voor de gebruiker ".$user['username']." (".$user['name'].") het persoonlijke doel aangepast naar: " . $doel);
                $_SESSION['doel_changed'] = 'true';
            }
        }
        if(isset($_POST['updateDoel_Global']) && is_numeric($_POST['updateDoel_Global']) && $_POST['updateDoel_Global'] >= 0){
            $doel = round($_POST['updateDoel_Global'],1);
            $org = getOrganisation($_SESSION['org']);
            if(orgExistsByID($id)){
                foreach(getAllUsersFromOrg($id) as $user) {
                    if (isAdmin($user['id'])) continue;
                    if ($user['active'] == 0) continue;
                    setPersoonlijkeDoel($_SESSION['org'], $user['id'], $doel);
                    addLog($_SESSION['user_id'], "Succesvolle voor de organisatie ". $org["name"] . " (".$org['location'].") voor de gebruiker ".$user['username']." (".$user['name'].") het persoonlijke doel aangepast naar: " . $doel);
                }
                $_SESSION['doel_changed'] = 'true';
            }
        }
    }

    if(!isset($_SESSION['doel_changed'])){
        $_SESSION['doel_changed'] = 'false';
    }
}
?>

<?php require_once './includes/modules/org_overzicht_doel_header.php'; ?>
<?php if(!isAdmin($_SESSION['user_id']) && hasPerms($_SESSION['org'],$_SESSION['user_id'], "page.overzicht.doel.manage")){ ?>
    <?php require_once './includes/modules/org_overzicht_doel_org.php'; ?>
<?php } ?>
<?php require_once './includes/modules/org_overzicht_doel_table.php'; ?>
