<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php require_once './includes/modules/admin_logboek_header.php'; ?>
<?php if(isset($request[2]) && is_numeric($request[2]) && userExistsByUserID($request[2]) && !empty(getLogs($request[2]))){ ?>
    <?php require_once './includes/modules/admin_logboek_information.php'; ?>
<?php }else{ ?>
    <?php require_once './includes/modules/admin_logboek_users.php'; ?>
<?php } ?>
