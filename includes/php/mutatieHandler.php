<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function getMutatieHandler($org, $type){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM mutaties WHERE org_id = ? AND type = ? ORDER BY time DESC")) {
        $stmt->bind_param('is', $org, $type);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function getMutatie($id){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM mutaties WHERE id = ?")) {
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows[0];
    }
    return array();
}

function addMutatie($org, $type, $total, $omschrijving){
    global $mysqli;
    $now = time();
    if ($stmt = $mysqli->prepare("INSERT INTO mutaties (org_id, type, time, total, omschrijving) VALUES (?,?,?,?,?)")) {
        $stmt->bind_param('issds', $org,$type,$now,$total,$omschrijving);
        $stmt->execute();
        $stmt->store_result();
    }
}

function addMutatieLog($org, $total, $kas){
    $id = getOrganisationMutatieID($org)['MutatieID'];
    updateOrganisationLastMutatieID($org);
    $time = time();
    global $mysqli;
    if(mutatieLogExists($org, $id)){
        $stmt = $mysqli->prepare("UPDATE mutatieLogs SET omzet = ?, time = ?, kas = ? WHERE org_id = ? AND time_id = ?");
        $stmt->bind_param('dsdii', $total,$time,$kas,$org, $id);
        $stmt->execute();
    }else{
        $stmt = $mysqli->prepare("INSERT INTO mutatieLogs (org_id, time_id, omzet, time, kas) VALUES (?,?,?,?,?)");
        $stmt->bind_param('iidsd', $org,$id, $total, $time, $kas);
        $stmt->execute();
    }
}

function mutatieLogExists($id, $time){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM mutatieLogs WHERE org_id = ? AND time_id = ?  LIMIT 1")) {
        $stmt->bind_param('ii', $id, $time);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows != 1) {
            return false;
        }else{
            return true;
        }
    }
}

function removeMutatie($id){
    global $mysqli;
    if ($stmt = $mysqli->prepare("DELETE FROM mutaties WHERE id = ?")) {
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->store_result();
    }
}

function getTotaleMutaties($org, $type){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT SUM(total) as total FROM mutaties WHERE org_id = ? AND type = ?")) {
        $stmt->bind_param('is', $org, $type);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        if(isset($allRows[0])){
            return $allRows[0];
        }
    }
    return 0;
}

function mutatieExists($id){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM mutaties WHERE id = ? LIMIT 1")) {
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows != 1) {
            return false;
        }else{
            return true;
        }
    }
}

function getLastMutatieLogs($org){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT org_id, time_id, omzet,kas,time FROM mutatieLogs WHERE org_id = ? ORDER BY time_id DESC LIMIT 12")) {
        $stmt->bind_param('i', $org);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}