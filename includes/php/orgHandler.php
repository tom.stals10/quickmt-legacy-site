<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

include_once 'userInfo.php';

function getAllOrganisations(){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM organisation LEFT JOIN organisationStats ON org_id = id")) {
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function getOrganisation($id){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM organisation LEFT JOIN organisationStats ON org_id = id WHERE id = ? LIMIT 1")) {
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows[0];
    }
    return array();
}

function setOrgBelastingPercentage($id, $percentage){
    global $mysqli;
    $stmt = $mysqli->prepare("UPDATE organisationStats SET belastingPercentage = ? WHERE org_id = ?");
    $stmt->bind_param('ii', $percentage,$id);
    $stmt->execute();
}

function getOrgBelastingPercentage($id){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT belastingPercentage FROM organisationStats WHERE org_id = ? LIMIT 1")) {
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        if(isset($allRows[0]['belastingPercentage'])) {
            return $allRows[0]['belastingPercentage'];
        }
    }
    return 0;
}

function getOrganisationIDByName($name){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT id FROM organisation WHERE name = ? LIMIT 1")) {
        $stmt->bind_param('s', $name);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows[0];
    }
    return array();
}

function getOrganisationMutatieID($id){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT MutatieID FROM organisationStats WHERE org_id = ? LIMIT 1")) {
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows[0];
    }
    return array();
}

function updateOrganisationLastMutatieID($id){
    $oldid = getOrganisationMutatieID($id)['MutatieID'];
    $newid = $oldid + 1;
    global $mysqli;
    $stmt = $mysqli->prepare("UPDATE organisationStats SET MutatieID = ? WHERE org_id = ?");
    $stmt->bind_param('ii', $newid,$id);
    $stmt->execute();
}


function createOrganisation($name, $location, $partner){
    global $mysqli;
    $name = strip_tags($name);
    $now = time();
    $location = strip_tags($location);
    $stmt = $mysqli->prepare("INSERT INTO organisation (name,location, partner) VALUES (?,?,?)");
    $stmt->bind_param('ssi', $name,$location,$partner);
    $stmt->execute();
    $orgID = getOrganisationIDByName($name)['id'];
    $adminID = $_SESSION['user_id'];
    $stmt = $mysqli->prepare("INSERT INTO organisationStats (org_id,createdOn,createdBy,lastNameChange,lastLocationChange) VALUES (?,?,?,?,?)");
    $stmt->bind_param('issss', $orgID,$now,$adminID,$now,$now);
    $stmt->execute();
    if($partner == 1){
        setOrgBelastingPercentage($orgID, 5);
    }
}

function changeOrganisationName($org, $name){
    global $mysqli;
    $name = strip_tags($name);
    if(orgExistsByName($name)){
        return;
    }
    $stmt = $mysqli->prepare("UPDATE organisation SET name = ? WHERE id = ?");
    $stmt->bind_param('si', $name,$org);
    $stmt->execute();
    $now = time();
    $stmt = $mysqli->prepare("UPDATE organisationStats SET lastNameChange = ? WHERE org_id = ?");
    $stmt->bind_param('si', $now,$org);
    $stmt->execute();
}

function changeOrganisationLocation($org, $location){
    global $mysqli;
    $location = strip_tags($location);
    $stmt = $mysqli->prepare("UPDATE organisation SET location = ? WHERE id = ?");
    $stmt->bind_param('si', $location,$org);
    $stmt->execute();
    $now = time();
    $stmt = $mysqli->prepare("UPDATE organisationStats SET lastLocationChange = ? WHERE org_id = ?");
    $stmt->bind_param('si', $now,$org);
    $stmt->execute();
}

function changeOrganisationIcon($org, $icon){
    global $mysqli;
    $icon = strip_tags($icon);
    $stmt = $mysqli->prepare("UPDATE organisation SET icon = ? WHERE id = ?");
    $stmt->bind_param('si', $icon,$org);
    $stmt->execute();
    $now = time();
    $stmt = $mysqli->prepare("UPDATE organisationStats SET lastIconChange = ? WHERE org_id = ?");
    $stmt->bind_param('si', $now,$org);
    $stmt->execute();
}

function deactiveOrganisation($org){
    global $mysqli;
    $active = 0;
    $stmt = $mysqli->prepare("UPDATE organisation SET active = ? WHERE id = ?");
    $stmt->bind_param('ii', $active,$org);
    $stmt->execute();
    $now = time();
    $adminID = $_SESSION['user_id'];
    $stmt = $mysqli->prepare("UPDATE organisationStats SET deletedOn = ?,deletedBy = ? WHERE org_id = ?");
    $stmt->bind_param('sii', $now,$adminID,$org);
    $stmt->execute();
}

function setFantasy($org, $status){
    global $mysqli;
    if($status == 1 || $status == 0){
        $stmt = $mysqli->prepare("UPDATE organisation SET fantasy = ? WHERE id = ?");
        $stmt->bind_param('ii', $status,$org);
        $stmt->execute();
    }
}

function setPartner($org, $status){
    global $mysqli;
    if($status == 1 || $status == 0){
        $stmt = $mysqli->prepare("UPDATE organisation SET partner = ? WHERE id = ?");
        $stmt->bind_param('ii', $status,$org);
        $stmt->execute();
    }
}

function hasAccess($orgID, $userId){
    global $mysqli;
    if(isAdmin($userId)){
        return true;
    }
    if ($stmt = $mysqli->prepare("SELECT user_id FROM userOrganisation WHERE user_id = ?  AND org_id = ?")) {
        $stmt->bind_param('ii', $userId, $orgID);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            return true;
        }
    }
    return false;
}

function orgExistsByName($name){
    global $mysqli;
    $name = strip_tags($name);
    if ($stmt = $mysqli->prepare("SELECT name FROM organisation WHERE name = ? LIMIT 1")) {
        $stmt->bind_param('s', $name);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows != 1) {
            return false;
        }else{
            return true;
        }
    }
}

function orgExistsByID($org){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT name FROM organisation WHERE id = ? LIMIT 1")) {
        $stmt->bind_param('i', $org);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows != 1) {
            return false;
        }else{
            return true;
        }
    }
}

function totaalAantalOrganisaties(){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT name FROM organisation")) {
        $stmt->execute();
        $stmt->store_result();
        return $stmt->num_rows;
    }
    return 0;
}

function isActiveOrg($orgID){
    global $mysqli;
    $active = 1;
    if ($stmt = $mysqli->prepare("SELECT active FROM organisation WHERE active = ?  AND id = ?")) {
        $stmt->bind_param('ii', $active , $orgID);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows != 1) {
            return false;
        }else{
            return true;
        }
    }
}

function isPartner($orgID){
    global $mysqli;
    $partner = 1;
    if ($stmt = $mysqli->prepare("SELECT partner FROM organisation WHERE partner = ?  AND id = ?")) {
        $stmt->bind_param('ii', $partner , $orgID);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows != 1) {
            return false;
        }else{
            return true;
        }
    }
}


function isFantasy($orgID){
    global $mysqli;
    $fantasy = 1;
    if ($stmt = $mysqli->prepare("SELECT fantasy FROM organisation WHERE fantasy = ?  AND id = ?")) {
        $stmt->bind_param('ii', $fantasy , $orgID);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows != 1) {
            return false;
        }else{
            return true;
        }
    }
}

function showOpen($orgID){
    global $mysqli;
    $showOpen = 1;
    if ($stmt = $mysqli->prepare("SELECT showOpen FROM organisation WHERE showOpen = ?  AND id = ?")) {
        $stmt->bind_param('ii', $showOpen , $orgID);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows != 1) {
            return false;
        }else{
            return true;
        }
    }
}

function setShowOpen($org, $status){
    global $mysqli;
    if($status == 1 || $status == 0){
        $stmt = $mysqli->prepare("UPDATE organisation SET showOpen = ? WHERE id = ?");
        $stmt->bind_param('ii', $status,$org);
        $stmt->execute();
    }
}

function orgHasPage($orgID, $pageID){
    global $mysqli;
    if(isPartner($orgID)){
        if ($stmt = $mysqli->prepare("SELECT * FROM orgPages WHERE org_id = ?  AND page_id = ? AND page_id IN (SELECT id FROM pages WHERE active = 1 AND partner = 1) LIMIT 1")) {
            $stmt->bind_param('ii', $orgID , $pageID);
            $stmt->execute();
            $stmt->store_result();
            if ($stmt->num_rows == 1) {
                return true;
            }else{
                return false;
            }
        }
    }else{
        if ($stmt = $mysqli->prepare("SELECT * FROM orgPages WHERE org_id = ?  AND page_id = ? AND page_id IN (SELECT id FROM pages WHERE active = 1 AND intern = 1) LIMIT 1")) {
            $stmt->bind_param('ii', $orgID , $pageID);
            $stmt->execute();
            $stmt->store_result();
            if ($stmt->num_rows == 1) {
                return true;
            }else{
                return false;
            }
        }
    }

    return false;
}

function orgPageEnable($orgID, $pageID){
    global $mysqli;
    if ($stmt = $mysqli->prepare("INSERT INTO orgPages (org_id, page_id) VALUES (?,?)")) {
        $stmt->bind_param('ii', $orgID , $pageID);
        $stmt->execute();
    }
}

function orgPageDisable($orgID, $pageID){
    global $mysqli;
    if ($stmt = $mysqli->prepare("DELETE FROM orgPages WHERE org_id = ? AND page_id = ?")) {
        $stmt->bind_param('ii', $orgID , $pageID);
        $stmt->execute();
    }
}

function getAllPages(){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM pages")) {
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function getPage($id){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM pages WHERE id = ? LIMIT 1")) {
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows[0];
    }
    return array();
}

function getAllUsersFromOrg($orgid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM user LEFT JOIN userStats on id = user_id WHERE id IN (SELECT user_id FROM userOrganisation WHERE org_id = ?) OR id IN (SELECT id FROM user WHERE admin = 1)")) {
        $stmt->bind_param('i', $orgid);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function addUserToOrg($orgid, $userid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("INSERT INTO userOrganisation (user_id, org_id) VALUES (?,?)")) {
        $stmt->bind_param('ii', $userid , $orgid);
        $stmt->execute();
    }
}

function deleteUserFromOrg($orgid, $userid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("INSERT INTO pendingUserDelete (org_id, user_id) VALUES (?,?)")) {
        $stmt->bind_param('ii', $orgid,$userid);
        $stmt->execute();
    }
}

function hasPendingDeletions($org){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT user_id FROM pendingUserDelete WHERE org_id = ? LIMIT 1")) {
        $stmt->bind_param('i', $org);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            return true;
        }
    }
    return false;
}

function deleteUserFromOrgAfterPending($org,$userid){
    global $mysqli;
    $stmt = $mysqli->prepare("DELETE FROM pendingUserDelete WHERE org_id = ? AND user_id = ?");
    $stmt->bind_param('ii', $org,$userid);
    $stmt->execute();

    $stmt = $mysqli->prepare("DELETE FROM userOrganisation WHERE org_id = ? AND user_id = ?");
    $stmt->bind_param('ii', $org,$userid);
    $stmt->execute();
}

function setLastLoonPayed($orgid, $time){
    global $mysqli;
    $stmt = $mysqli->prepare("UPDATE organisationStats SET lastLoonReset = ? WHERE org_id = ?");
    $stmt->bind_param('si', $time,$orgid);
    $stmt->execute();
}

function setLastVoorraadChecked($orgid, $userid){
    global $mysqli;
    $now = time();
    $stmt = $mysqli->prepare("UPDATE organisationStats SET lastVoorraadCheckedOn = ?, lastVoorraadCheckedBy = ?  WHERE org_id = ?");
    $stmt->bind_param('iii', $now,$userid,$orgid);
    $stmt->execute();
}

function setActive($org, $user){
    global $mysqli;
    $time = time();
    $stmt = $mysqli->prepare("UPDATE organisationStats SET lastActiveReport = ?, lastActiveReportBy = ? WHERE org_id = ?");
    $stmt->bind_param('sii', $time,$user,$org);
    $stmt->execute();
}
