<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function load($request){
    if(!isset($request[0])){
        require_once './includes/pages/home.php';
    }else {
        switch($request[0]){
            case 'profiel':
                require_once './includes/pages/profiel.php';
                break;
            case 'bestelling':
                require_once './includes/pages/bestelling.php';
                break;
            case 'melding':
                require_once './includes/pages/melding.php';
                break;
            case 'leaderboard':
                if(isPartner($_SESSION['org'])){
                    require_once './includes/pages/home.php';
                }else{
                    require_once './includes/pages/leaderboard.php';
                }
                break;
            case 'org':
                loadOrgs($request);
                break;
            case 'admin':
                loadAdmin($request);
                break;
            default:
                require_once './includes/pages/home.php';
                break;
        }
    }
}

function loadAdmin($request){
    if(!isset($request[1])){
        require_once './includes/pages/home.php';
    }else {
        switch($request[1]){
            case 'users':
                if(isAdmin($_SESSION['user_id']) && $_SESSION['org'] == 1) {
                    require_once './includes/pages/admin_users.php';
                }else{
                    require_once './includes/pages/home.php';
                }
                break;
            case 'org':
                if(isAdmin($_SESSION['user_id']) && $_SESSION['org'] == 1) {
                    require_once './includes/pages/admin_organisations.php';
                }else{
                    require_once './includes/pages/home.php';
                }
                break;
            case 'logs':
                if(isAdmin($_SESSION['user_id']) && $_SESSION['org'] == 1) {
                    require_once './includes/pages/admin_logs.php';
                }else{
                    require_once './includes/pages/home.php';
                }
                break;
            default:
                require_once './includes/pages/home.php';
                break;
        }
    }
}

function loadOrgs($request){
    if($_SESSION['org'] ==1){
        require_once './includes/pages/home.php';
    }else{
        if(!isset($request['1'])){
            require_once './includes/pages/home.php';
        }else {
            switch($request[1]){
                case 'admin':
                    loadOrgs_Admin($request);
                    break;
                case 'overzicht':
                    loadOrgs_Overzicht($request);
                    break;
                case 'verkoop':
                    if(hasPerms($_SESSION['org'],$_SESSION['user_id'],"page.verkoop") && orgHasPage($_SESSION['org'], 1) && !isAdmin($_SESSION['user_id'])){
                        require_once './includes/pages/org_verkoop.php';
                    }else{
                        require_once './includes/pages/home.php';
                    }
                    break;
                case 'voorraad':
                    if(hasPerms($_SESSION['org'],$_SESSION['user_id'],"page.voorraad") && orgHasPage($_SESSION['org'], 2) && !isAdmin($_SESSION['user_id'])){
                        require_once './includes/pages/org_voorraad.php';
                    }else{
                        require_once './includes/pages/home.php';
                    }
                    break;
                case 'notities':
                    if(hasPerms($_SESSION['org'],$_SESSION['user_id'],"page.notities") && orgHasPage($_SESSION['org'], 7) && !isAdmin($_SESSION['user_id'])){
                        require_once './includes/pages/org_notities.php';
                    }else{
                        require_once './includes/pages/home.php';
                    }
                    break;
                case 'planning':
                    if(hasPerms($_SESSION['org'],$_SESSION['user_id'],"page.planning") && orgHasPage($_SESSION['org'], 13) && !isAdmin($_SESSION['user_id'])){
                        require_once './includes/pages/org_planning.php';
                    }else{
                        require_once './includes/pages/home.php';
                    }
                    break;
                case 'afwezigheid':
                    if(hasPerms($_SESSION['org'],$_SESSION['user_id'],"page.afwezigheid") && orgHasPage($_SESSION['org'], 15) && !isAdmin($_SESSION['user_id'])){
                        require_once './includes/pages/org_afwezigheid.php';
                    }else{
                        require_once './includes/pages/home.php';
                    }
                    break;
                default:
                    require_once './includes/pages/home.php';
                    break;
            }
        }
    }
}

function loadOrgs_Admin($request){
    if(!isset($request[2])){
        require_once './includes/pages/home.php';
    }else {
        switch($request[2]){
            case 'pages':
                if(hasPerms($_SESSION['org'],$_SESSION['user_id'],"page.admin.pages")){
                    require_once './includes/pages/org_admin_page.php';
                }else{
                    require_once './includes/pages/home.php';
                }
                break;
            case 'users':
                if(hasPerms($_SESSION['org'],$_SESSION['user_id'],"page.admin.users")){
                    require_once './includes/pages/org_admin_user.php';
                }else{
                    require_once './includes/pages/home.php';
                }
                break;
            case 'products':
                if(hasPerms($_SESSION['org'],$_SESSION['user_id'],"page.admin.products")){
                    require_once './includes/pages/org_admin_product.php';
                }else{
                    require_once './includes/pages/home.php';
                }
                break;
            default:
                require_once './includes/pages/home.php';
                break;
        }
    }
}

function loadOrgs_Overzicht($request){
    if(!isset($request[2])){
        require_once './includes/pages/home.php';
    }else {
        switch($request[2]){
            case 'verkopen':
                if(hasPerms($_SESSION['org'],$_SESSION['user_id'],"page.overzicht.verkopen") && orgHasPage($_SESSION['org'], 5) && !isAdmin($_SESSION['user_id'])){
                    require_once './includes/pages/org_overzicht_verkoop.php';
                }else{
                    require_once './includes/pages/home.php';
                }
                break;
            case 'voorraad':
                if(hasPerms($_SESSION['org'],$_SESSION['user_id'],"page.overzicht.voorraad") && orgHasPage($_SESSION['org'], 3) && !isAdmin($_SESSION['user_id'])){
                    require_once './includes/pages/org_overzicht_voorraad.php';
                }else{
                    require_once './includes/pages/home.php';
                }
                break;
            case 'omzet':
                if(hasPerms($_SESSION['org'],$_SESSION['user_id'],"page.overzicht.omzet") && orgHasPage($_SESSION['org'], 4) && !isAdmin($_SESSION['user_id'])){
                    require_once './includes/pages/org_overzicht_omzet.php';
                }else{
                    require_once './includes/pages/home.php';
                }
                break;
            case 'lonen':
                if(hasPerms($_SESSION['org'],$_SESSION['user_id'],"page.overzicht.lonen") && orgHasPage($_SESSION['org'], 6) && !isAdmin($_SESSION['user_id'])){
                    require_once './includes/pages/org_overzicht_lonen.php';
                }else{
                    require_once './includes/pages/home.php';
                }
                break;
            case 'inkomsten_uitgaven':
                if(hasPerms($_SESSION['org'],$_SESSION['user_id'],"page.overzicht.in_out") && orgHasPage($_SESSION['org'], 8) && !isAdmin($_SESSION['user_id'])){
                    require_once './includes/pages/org_overzicht_in-out.php';
                }else{
                    require_once './includes/pages/home.php';
                }
                break;
            case 'doel':
                if(hasPerms($_SESSION['org'],$_SESSION['user_id'],"page.overzicht.doel") && orgHasPage($_SESSION['org'], 9) && !isAdmin($_SESSION['user_id'])){
                    require_once './includes/pages/org_overzicht_doel.php';
                }else{
                    require_once './includes/pages/home.php';
                }
                break;
            case 'bonus':
                if(hasPerms($_SESSION['org'],$_SESSION['user_id'],"page.overzicht.bonus") && orgHasPage($_SESSION['org'], 10) && !isAdmin($_SESSION['user_id'])){
                    require_once './includes/pages/org_overzicht_bonus.php';
                }else{
                    require_once './includes/pages/home.php';
                }
                break;
            case 'activiteit':
                if(hasPerms($_SESSION['org'],$_SESSION['user_id'],"page.overzicht.activity") && orgHasPage($_SESSION['org'], 11) && !isAdmin($_SESSION['user_id'])){
                    require_once './includes/pages/org_overzicht_activiteit.php';
                }else{
                    require_once './includes/pages/home.php';
                }
                break;
            case 'sollicitatie':
                if(hasPerms($_SESSION['org'],$_SESSION['user_id'],"page.overzicht.sollicitatie") && orgHasPage($_SESSION['org'], 14) && !isAdmin($_SESSION['user_id'])){
                    require_once './includes/pages/org_overzicht_sollicitatie.php';
                }else{
                    require_once './includes/pages/home.php';
                }
                break;
            case 'mededelingen':
                if(hasPerms($_SESSION['org'],$_SESSION['user_id'],"page.overzicht.mededelingen") && orgHasPage($_SESSION['org'], 12) && !isAdmin($_SESSION['user_id'])){
                    require_once './includes/pages/org_overzicht_mededelingen.php';
                }else{
                    require_once './includes/pages/home.php';
                }
                break;
            case 'afwezigheid':
                if(hasPerms($_SESSION['org'],$_SESSION['user_id'],"page.overzicht.afwezigheid") && orgHasPage($_SESSION['org'], 16) && !isAdmin($_SESSION['user_id'])){
                    require_once './includes/pages/org_overzicht_afwezigheid.php';
                }else{
                    require_once './includes/pages/home.php';
                }
                break;
            default:
                require_once './includes/pages/home.php';
                break;
        }
    }
}