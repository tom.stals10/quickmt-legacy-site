/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

$(document).ready(function() {
    $('#logboekTable').DataTable({
        searching: false,
        pageLength: 50,
        lengthChange: false,
        "language": {
            "info": "Pagina _PAGE_ van de _PAGES_",
            "paginate": {
                "next": "Volgende",
                "previous": "Vorige"
            }
        },
        "fnDrawCallback": function(oSettings) {
            if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
                $(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
                $(oSettings.nTableWrapper).find('.dataTables_info').hide();
            }
        },
        "ordering": false
    });
    ('#rechtenTable').DataTable({
        searching: true,
        pageLength: 50,
        lengthChange: false,
        "language": {
            "info": "Pagina _PAGE_ van de _PAGES_",
            "paginate": {
                "next": "Volgende",
                "previous": "Vorige"
            }
        },
        "fnDrawCallback": function(oSettings) {
            if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
                $(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
                $(oSettings.nTableWrapper).find('.dataTables_info').hide();
            }
        },
        "ordering": false
    });
    ('#adminOrgTable').DataTable({
        searching: false,
        pageLength: 100,
        lengthChange: false,
        "language": {
            "info": "Pagina _PAGE_ van de _PAGES_",
            "paginate": {
                "next": "Volgende",
                "previous": "Vorige"
            }
        },
        "fnDrawCallback": function(oSettings) {
            if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
                $(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
                $(oSettings.nTableWrapper).find('.dataTables_info').hide();
            }
        },
        "ordering": false
    });
});


