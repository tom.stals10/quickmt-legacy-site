/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';
function number_format(number, decimals, dec_point, thousands_sep) {
    // *     example: number_format(1234.56, 2, ',', ' ');
    // *     return: '1 234,56'
    number = (number + '').replace(',', '').replace(' ', '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function(n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

function get_kas(id){
    var kas = localStorage.getItem('kas_'+id);
    return kas;
}

function get_omzet(id){
    var kas = localStorage.getItem('omzet_'+id);
    return kas;
}

function getLabels(id){
  
    var time = localStorage.getItem('time_'+id);
    if(time == 0){
        return '1 12 2020';
    }
     
    var date = new Date(time * 1000);
    
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    
    var day = date.getDate();
    
    console.log(date)
    
    return day + ' ' + months[date.getMonth()] + ' ' + date.getFullYear();
}

// Area Chart Example
var ctx = document.getElementById("myAreaChart");
var in_1 = localStorage.getItem('kas_1_in');
var myLineChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [
            getLabels(1),
            getLabels(2),
            getLabels(3),
            getLabels(4),
            getLabels(5),
            getLabels(6),
            getLabels(7),
            getLabels(8),
            getLabels(9),
            getLabels(10),
            getLabels(11),
            getLabels(12),
        ],
        datasets: [{
            label: "Kas",
            lineTension: 0.3,
            backgroundColor: "rgba(78, 115, 223, 0.05)",
            borderColor: "rgb(111,223,132)",
            pointRadius: 3,
            pointBackgroundColor: "rgb(111,223,132)",
            pointBorderColor: "rgb(111,223,132)",
            pointHoverRadius: 3,
            pointHoverBackgroundColor: "rgb(111,223,132)",
            pointHoverBorderColor: "rgb(111,223,132)",
            pointHitRadius: 10,
            pointBorderWidth: 2,
            data: [
                get_kas(1),
                get_kas(2),
                get_kas(3),
                get_kas(4),
                get_kas(5),
                get_kas(6),
                get_kas(7),
                get_kas(8),
                get_kas(9),
                get_kas(10),
                get_kas(11),
                get_kas(12)
            ],
        },{
            label: "Omzet",
            lineTension: 0.3,
            backgroundColor: "rgba(78, 115, 223, 0.05)",
            borderColor: "rgb(77,91,223)",
            pointRadius: 3,
            pointBackgroundColor: "rgb(77,91,223)",
            pointBorderColor: "rgb(77,91,223)",
            pointHoverRadius: 3,
            pointHoverBackgroundColor: "rgb(77,91,223)",
            pointHoverBorderColor: "rgb(77,91,223)",
            pointHitRadius: 10,
            pointBorderWidth: 2,
            data: [
                get_omzet(1),
                get_omzet(2),
                get_omzet(3),
                get_omzet(4),
                get_omzet(5),
                get_omzet(6),
                get_omzet(7),
                get_omzet(8),
                get_omzet(9),
                get_omzet(10),
                get_omzet(11),
                get_omzet(12)
            ],
        }],
    },
    options: {
        maintainAspectRatio: false,
        layout: {
            padding: {
                left: 10,
                right: 25,
                top: 25,
                bottom: 0
            }
        },
        scales: {
            xAxes: [{
                time: {
                    unit: 'date'
                },
                gridLines: {
                    display: false,
                    drawBorder: false
                },
                ticks: {
                }
            }],
            yAxes: [{
                ticks: {
                    maxTicksLimit: 5,
                    padding: 10,
                    // Include a dollar sign in the ticks
                    callback: function(value, index, values) {
                        return '€' + number_format(value);
                    }
                },
                gridLines: {
                    color: "rgb(234, 236, 244)",
                    zeroLineColor: "rgb(234, 236, 244)",
                    drawBorder: false,
                    borderDash: [2],
                    zeroLineBorderDash: [2]
                }
            }],
        },
        legend: {
            display: false
        },
        tooltips: {
            backgroundColor: "rgb(255,255,255)",
            bodyFontColor: "#858796",
            titleMarginBottom: 10,
            titleFontColor: '#6e707e',
            titleFontSize: 14,
            borderColor: '#dddfeb',
            borderWidth: 1,
            xPadding: 15,
            yPadding: 15,
            displayColors: false,
            intersect: false,
            mode: 'index',
            caretPadding: 10,
            callbacks: {
                label: function(tooltipItem, chart) {
                    var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                    return datasetLabel + ': €' + number_format(tooltipItem.yLabel);
                }
            }
        }
    }
});
