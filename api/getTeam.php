<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

include_once '../includes/auth/db_connect.php';
include_once '../includes/auth/functions.php';
include_once '../includes/php/productHandler.php';
include_once '../includes/php/orgHandler.php';
include_once '../includes/php/permHandler.php';

$output = array();

if(isset($_GET['org'])){
    $_GET['org'] = str_replace("_", " ", $_GET['org']);
    if(orgExistsByName($_GET['org']) && !isFantasy($_GET['org'])&& !isPartner($_GET['org'])){
        $orgID = getOrganisationIDByName($_GET['org'])['id'];
        if(isActiveOrg($orgID)){
            foreach(getAllUsersFromOrg($orgID) as $user){
                if(isAdmin($user['id'])) continue;
                if($user['active'] == 0) continue;
                if(hasPerms($orgID, $user['id'], "loon.percentage.0")){
                    $output[$user['name']] = "Eigenaar";
                }elseif(hasPerms($orgID, $user['id'], "loon.percentage.125")){
                    $output[$user['name']] = "Manager";
                }elseif(hasPerms($orgID, $user['id'], "loon.percentage.11")){
                    $output[$user['name']] = "Inspecteur";
                }else{
                    $output[$user['name']] = "Medewerker";
                }
            }
        }
    }
}

print json_encode($output, JSON_UNESCAPED_UNICODE);
