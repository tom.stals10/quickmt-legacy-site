<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

include_once '../includes/auth/db_connect.php';
include_once '../includes/auth/functions.php';
include_once '../includes/php/orgHandler.php';
include_once '../includes/php/mutatieHandler.php';

$output = array(
    1 => '0=0=0',
    2 => '0=0=0',
    3 => '0=0=0',
    4 =>'0=0=0',
    5 =>'0=0=0',
    6 => '0=0=0',
    7 => '0=0=0',
    8 => '0=0=0',
    9 => '0=0=0',
    10 => '0=0=0',
    11 => '0=0=0',
    12 => '0=0=0',
);

if(isset($_GET['org']) && orgExistsByID($_GET['org'])) {
    $logs = getLastMutatieLogs($_GET['org']);
    $i = 12;
    foreach ($logs as $log){
        $output[$i] = $log['omzet'] . '=' . $log['time']. '=' . $log['kas'];
        $i--;
    }
}
print json_encode($output,JSON_UNESCAPED_UNICODE);
