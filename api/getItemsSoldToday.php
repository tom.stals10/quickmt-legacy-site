<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

include_once '../includes/auth/db_connect.php';
include_once '../includes/auth/functions.php';
include_once '../includes/php/productHandler.php';
include_once '../includes/php/orgHandler.php';
include_once '../includes/php/permHandler.php';
include_once '../includes/php/verkoopHandler.php';

$output = array();

$producten = array();

if(isset($_GET['org'],$_GET['user']) && orgExistsByID($_GET['org']) && !isFantasy($_GET['org'])){
    $username = strtolower($_GET['user']);
    foreach (getAllProducts($_GET['org']) as $product){
        if(!productIsDailyLimited($product['id'])) continue;
        $producten[$product['id']] = getProductLimit($product['id']);
    }

    $verkopen = getAllVerkopenFromUserToday($_GET['org'],$username);
    foreach ($verkopen as $verkoop){
        $productenVerkoop = json_decode($verkoop['producten'],true);
        foreach ($productenVerkoop as $productid => $aantal){
            if(array_key_exists($productid,$producten)){
                $producten[$productid] -= $aantal;
            }
        }
    }


    foreach ($producten as $id => $aantal){
        $output[$id] = array($aantal, getProductByID($id)['name']);
    }
}

print json_encode($output,JSON_UNESCAPED_UNICODE);
