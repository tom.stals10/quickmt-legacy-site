QuickMT - Legacy Portal
======================
Waarom open source?
-------------------
Ik heb deze website ooit ontwikkeld voor een winkel in de game Minetopia.

Ik heb besloten om de code openbaar te maken, zodat anderen wanneer ze hier interesse in hebben kunnen zien hoe het portaal al die tijd heeft gewerkt en misschien zelf hier van kunnen leren.

Gaan er nog updates komen?
--------------------------
Nee, deze code is `as is` maar voel je vrij om een fork te maken en er verder mee te werken. Respecteer daarbij wel de license!

Wat vindt je zelf van de code?
--------------------------
Ik heb deze code best een tijd geleden gemaakt en ik sta er zelf niet 100% achter hoe deze code is geworden. 
Ik heb deze code in mijn vrije tijd en als experiment voor de lol ontwikkeld en hierin zijn design- en ontwikkel keuzes gemaakt die ik als ik er nu op terug kijk niet zou maken.
Toch heb ik er voor gekozen deze code wel openbaar te maken voor de mensen die er interesse in hebben.

Hoe krijg ik de code werkend?
--------------------------
1. Copy alle code naar de je webserver
2. Importeer het SQL bestand in je database
3. Pas in .env.php de credentials van je databas aan
4. Je kan nu inloggen met als gebruiker `admin`, wachtwoord `test` en persoonlijke code `123456`
5. Selecteer als bedrijf `Admin - Cloud` (Dit is het administratie bedrijf waar je gebruikers aan kunt maken, bedrijven aan kunt maken en audit logboeken kunt inzien)
6. Vragen of problemen? Je kunt me altijd bereiken op discord: `ItsJulian#8581`
